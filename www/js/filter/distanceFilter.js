/**
 * Created by rbn-imac on 7/7/15.
 */
angular.module('starter.filters', []).filter('distance', function() {
    return function(input,lat1,long1,lat2,long2) {
        //function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
        //    var R = 6371; // Radius of the earth in km
        //    var dLat = deg2rad(lat2-lat1);  // deg2rad below
        //    var dLon = deg2rad(lon2-lon1);
        //    var a =
        //            Math.sin(dLat/2) * Math.sin(dLat/2) +
        //            Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        //            Math.sin(dLon/2) * Math.sin(dLon/2)
        //        ;
        //    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        //    var d = R * c; // Distance in km
        //    return d;
        //}
        //
        //function deg2rad(deg) {
        //    return deg * (Math.PI/180)
        //}
        //return getDistanceFromLatLonInKm(lat1,long1,lat2,long2);


        function calcCrow(lat1, lon1, lat2, lon2)
        {
            var R = 6371; // km
            var dLat = toRad(lat2-lat1);
            var dLon = toRad(lon2-lon1);
            var lat1 = toRad(lat1);
            var lat2 = toRad(lat2);

            var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            var d = R * c;
            //float rtn = Number.pad;
            return d.toFixed(2);
        }

        // Converts numeric degrees to radians
        function toRad(Value)
        {
            return Value * Math.PI / 180;
        }
        return calcCrow(lat1,long1,lat2,long2);
    };
}).filter('fareCalculate', function() {

    // In the return function, we must pass in a single parameter which will be the data we will work on.
    // We have the ability to support multiple other parameters that can be passed into the filter optionally
    return function(input) {

        var output = input*30;

        // Do filter work here

        return output;

    }

});