// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ionic.rating', 'ngCordova','ngCordovaOauth', 'xeditable', 'pascalprecht.translate', 'ngMap', 'starter.controllers', 'starter.services','starter.filters', 'angular-svg-round-progress'])

    .run(function ($ionicPlatform, $translate, $rootScope,$ionicPopup, $state,  LocalService, $ionicLoading, $ionicPopup, $timeout, $ionicHistory) {
        console.log("ekhane aise");
        $ionicPlatform.registerBackButtonAction(function (event) {
            console.log("ekhane aise back");
            if($ionicHistory.currentStateName() == "myiew"){
                ionic.Platform.exitApp();
                // or do nothing
            }
            else {
                $ionicHistory.goBack();
            }
        }, 100);
       
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams,  $ionicHistory) {

     console.log(fromState.name);
     console.log(toState.name);

            if(toState.name == 'app.passenger_home' ){
                $rootScope.goToHomeReset();
                //$ionicHistory.nextViewOptions({
                //    //disableAnimate: true,
                //    disableBack: true
                //});
                if(fromState.name == 'app.passenger_pickup_point' || fromState.name == 'app.passenger_destination_point' ){
                    $rootScope.goToHomeReset();
                }else if(fromState.name == 'app.passenger-find-driver'){

                }
                else{
                    $rootScope.mapReset();
                }

            }
            if(fromState.name == 'app.passenger_home' && toState.name == 'app.passenger_destination_point'){


            }
            if(toState.name == 'app.passenger_pickup_point'){
                $rootScope.setMarkerOnPickup();

            }
            if(toState.name == 'app.passenger_destination_point'){
                $rootScope.setMarkerOnDestination();

            }


        });

        function checkConnection() {
            var networkState = navigator.connection.type;

            var states = {};
            states[Connection.UNKNOWN]    = 'Unknown connection';
            states[Connection.ETHERNET]    = 'Ethernet connection';
            states[Connection.WIFI]       = 'WiFi connection';
            states[Connection.CELL_2G]    = 'Cell 2G connection';
            states[Connection.CELL_3G]    = 'Cell 3G connection';
            states[Connection.CELL_4G]    = 'Cell 4G connection';
            states[Connection.NONE]       = 'No network connection';

            //alert('Connection type: ' + states[networkState]);
        }
        function onDeviceReady() {
            checkConnection();
        }


        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }




        });

        if (localStorage.getItem('lang')){
            $translate.use(localStorage.getItem('lang'));
        }
        else
        {
            localStorage.setItem('lang', 'bn_BN');
            $translate.use('bn_BN');
        }

        $rootScope.changeLanguage = function (langKey) {
            $translate.use(langKey);
            localStorage.setItem('lang', langKey);
        };
        $rootScope.ionicLoadingShow = function() {
            $ionicLoading.show({
                template: ' <div class="spinner"> <div class="cube1"></div> <div class="cube2"></div> </div>'
            });
        };
        $rootScope.ionicLoadingHide = function(){
            $ionicLoading.hide();
        };

        $rootScope.showAlert = function(title, template) {
            var alertPopup = $ionicPopup.alert({
                title: title,
                template: template
            });
            alertPopup.then(function(res) {
                //console.log('Thank you for not eating my delicious ice cream cone');
            });
            $timeout(function() {
                alertPopup.close(); //close the popup after 2.5 seconds for some reason
            }, 2500);
        };

        document.addEventListener("deviceready", onDeviceReady, false);

    });

//app.config([
//    "$routeProvider",
//    "$httpProvider",
//    function($routeProvider, $httpProvider){
//        $httpProvider.defaults.headers.common['Access-Control-Allow-Headers'] = '*';
//    }
//]);