/**
 * Created by rbn on 5/13/15.
 */
angular.module('starter')
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
//--------------------------driver login & register ----------------------------------------
            .state('driver_login', {
                url: "/driver-login",
                templateUrl: "templates/driver/driver_login.html",
                controller: 'DriverLoginController'
            })
            .state('driver_register', {
                url: "/driver-register",
                templateUrl: "templates/driver/driver_register.html",
                controller: 'DriverRegisterController'
            })
// -------------------------- passenger login & register --------------------------------------
            .state('passenger_login', {
                url: "/passenger-login",
                templateUrl: "templates/passenger/passenger_login.html",
                controller: 'PassengerLoginController'
                //data: {
                //    access: AccessLevels.anon
                //}
            })
            .state('passenger_register', {
                url: "/passenger-register",
                templateUrl: "templates/passenger/passenger_register.html",
                controller: 'PassengerRegisterController'
            })
// -------------------------------------------------------------------------------------------
            .state('app', {
                url: "/app",
                abstract: true,
                templateUrl: "templates/menu.html",
                controller: 'AppController'
            })

            .state('app.settings', {
                url: "/settings",
                views: {
                    'menuContent': {
                        templateUrl: "templates/menu/settings.html",
                        controller: 'SettingsController'
                    }
                }
            })

            .state('app.contactsupport', {
                url: "/contact-support",
                views: {
                    'menuContent': {
                        templateUrl: "templates/menu/settings/contact-support.html"
                       // controller: 'SettingsController'
                    }
                }
            })
            .state('app.passenger_riding_history', {
                url: "/passenger-riding-history",
                views: {
                    'menuContent': {
                        templateUrl: "templates/passenger/riding_history.html",
                        controller: 'PassengerRidingHistoryController'
                    }
                }
            })
            .state('app.passenger_riding_details', {
                url: "/passenger-riding-details/:booking_id",
                views: {
                    'menuContent': {
                        templateUrl: "templates/passenger/riding_details.html",
                        controller: 'PassengerRidingDetailsController'
                    }
                }
            })

            .state('app.profile', {
                url: "/profile",
                views: {
                    'menuContent': {
                        templateUrl: "templates/menu/settings/profile.html",
                        controller: 'ProfileController'
                    }
                }
            })
            .state('app.password', {
                url: "/password",
                views: {
                    'menuContent': {
                        templateUrl: "templates/menu/settings/password.html",
                        controller: 'PasswordController'
                    }
                }
            })
//------------------------------------------------------------------------------------------
            .state('app.search', {
                url: "/search",
                views: {
                    'menuContent': {
                        templateUrl: "templates/search.html"
                    }
                }
            })

            .state('app.browse', {
                url: "/browse",
                views: {
                    'menuContent': {
                        templateUrl: "templates/browse.html"
                    }
                }
            })
            .state('app.playlists', {
                url: "/playlists",
                views: {
                    'menuContent': {
                        templateUrl: "templates/playlists.html",
                        controller: 'PlaylistsCtrl'
                    }
                }
            })
            .state('app.single', {
                url: "/playlists/:playlistId",
                views: {
                    'menuContent': {
                        templateUrl: "templates/playlist.html",
                        controller: 'PlaylistCtrl'
                    }
                }
            })
//----------------------------------------- driver start ----------------------------------------------
            .state('driver_app', {
                url: "/driver_app",
                abstract: true,
                templateUrl: "templates/driver_menu.html",
                controller: 'DriverAppController'
            })
            .state('driver_app.driver_home', {
                url: "/driver-home",
                views: {
                    'menuContent': {
                        templateUrl: "templates/driver/driver_home.html",
                        controller: 'DriverHomeController'
                    }
                }
            })
            .state('driver_app.driver_message_received', {
                url: "/driver-message-received",
                views: {
                    'menuContent': {
                        templateUrl: "templates/driver/driver_message_received.html",
                        controller: 'DriverMessageReceivedController'
                    }
                }
            })
           .state('driver_app.driver_reached_destination', {
                url: "/driver_reached_destination",
                views: {
                    'menuContent': {
                        templateUrl: "templates/driver/driver_reached_destination.html",
                        controller: 'DriverReachedDestinationController'
                    }
                }
            })
            .state('driver_app.driver_contact_passenger', {
                url: "/driver-contact-passenger",
                views: {
                    'menuContent': {
                        templateUrl: "templates/driver/driver_contact_passenger.html",
                        controller: 'DriverContactPassengerController'
                    }
                }
            })
            .state('driver_app.driver_riding_history', {
                url: "/driver-riding-history",
                views: {
                    'menuContent': {
                        templateUrl: "templates/driver/riding_history.html",
                        controller: 'DriverRidingHistoryController'
                    }
                }
            })
            .state('driver_app.driver_riding_details', {
                url: "/driver-riding-details/:booking_id",
                views: {
                    'menuContent': {
                        templateUrl: "templates/driver/riding_details.html",
                        controller: 'DriverRidingDetailsController'
                    }
                }
            })
            .state('driver_app.settings', {
                url: "/settings",
                views: {
                    'menuContent': {
                        templateUrl: "templates/menu/settings.html",
                        controller: 'SettingsController'
                    }
                }
            })

            .state('driver_app.contactsupport', {
                url: "/contact-support",
                views: {
                    'menuContent': {
                        templateUrl: "templates/menu/settings/contact-support.html"
                        // controller: 'SettingsController'
                    }
                }
            })
// -------------------------------------- driver end ---------------------------------------------
//---------------------------------------passenger start -----------------------------------------
            .state('app.passenger_home', {
                url: "/passenger-home",
                views: {
                    'menuContent': {
                        templateUrl: "templates/passenger/passenger_home.html",
                        controller: 'PassengerHomeController'
                    }
                }
            })
            .state('app.passenger_ride_type', {
                url: "/passenger-ride-type",
                views: {
                    'menuContent': {
                        templateUrl: "templates/passenger/passenger_ride_type.html",
                        controller: 'PassengerRideTypeController'
                    }
                }
            })
            .state('app.passenger_pickup_point', {
                url: "/passenger-pickup-point",
                views: {
                    'menuContent': {
                        templateUrl: "templates/passenger/passenger_pickup_point.html",
                        controller: 'PassengerPickupPointController'
                    }
                }
            })
            .state('app.passenger_destination_point', {
                url: "/passenger-destination-point",
                views: {
                    'menuContent': {
                        templateUrl: "templates/passenger/passenger_destination_point.html",
                        controller: 'PassengerDestinationPointController'
                    }
                }
            })
            .state('app.passenger_select_driver', {
                url: "/passenger-select-driver",
                views: {
                    'menuContent': {
                        templateUrl: "templates/passenger/passenger_select_driver.html",
                        controller: 'PassengerSelectDriverController'
                    }
                }
            })
            .state('app.passenger_find_driver', {
                url: "/passenger-find-driver",
                views: {
                    'menuContent': {
                        templateUrl: "templates/passenger/passenger_find_driver.html",
                        controller: 'PassengerFindDriverController'
                    }
                }
            })
            .state('app.passenger_contancting_driver', {
                url: "/passenger-contacting-driver",
                views: {
                    'menuContent': {
                        templateUrl: "templates/passenger/passenger_contacting_driver.html",
                        controller: 'PassengerContactingDriverController'
                    }
                }
            })
            .state('app.passenger_driver_responded', {
                url: "/passenger-driver-responded",
                views: {
                    'menuContent': {
                        templateUrl: "templates/passenger/passenger_driver_responded.html",
                        controller: 'PassengerDriverRespondedController'
                    }
                }
            })
            .state('app.passenger_track_driver', {
                url: "/passenger-track-driver",
                views: {
                    'menuContent': {
                        templateUrl: "templates/passenger/passenger_track_driver.html",
                        controller: 'PassengerTrackDriverController'
                    }
                }
            })
            .state('app.passenger_reached_destination', {
                url: "/passenger_reached_destination",
                views: {
                    'menuContent': {
                        templateUrl: "templates/passenger/passenger_reached_destination.html",
                        controller: 'PassengerReachedDestinationController'
                    }
                }
            })
            .state('camera', {
                url: "/camera",


                        templateUrl: "templates/menu/camera.html",
                        controller: 'CameraController'


            });
//driver_registe, driver_home, driver_message_received, driver_contact_passenger
//passenger_login, passenger_registration, passenger_home,
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/passenger-login');
        //$urlRouterProvider.otherwise('/camera');
    });
