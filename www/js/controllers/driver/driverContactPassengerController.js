/**
 * Created by rbn on 5/14/15.
 */
angular.module('starter')

    .controller('DriverContactPassengerController', function($scope, $stateParams,  $timeout) {

        $scope.loader = true;
        $timeout(function() {
           $scope.loader = false;
        }, 3000);
    });
