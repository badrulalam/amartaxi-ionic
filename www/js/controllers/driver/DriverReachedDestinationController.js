/**
 * Created by rbn on 5/14/15.
 */
angular.module('starter')

    .controller('DriverReachedDestinationController', function($scope,$cordovaGeolocation,  $stateParams, $rootScope, XmppAuth, $interval, Booking, $ionicPopup, $state) {
      console.log($rootScope.passengerInfo);
      $scope.on_message = function (message) {
        console.log(message);
      }
      if($rootScope.passengerInfo)
      {
        if(XmppAuth.connection){
          XmppAuth.connection.addHandler($scope.on_message, null, "message", "chat");
          console.log("xmpp connected");
        }
        else {
          console.log("xmpp not not connected");
        }
      }
      else {
        console.log("rootscope passenger info not found");
      }

        $scope.originCoord = {};
        $scope.destinationCoord = {};
        $scope.originCoord.lat= 23.792496;
        $scope.originCoord.long= 90.407806;
        $scope.destinationCoord.lat= 23.746465;
        $scope.destinationCoord.long= 90.376013;
        // $scope.originCoord = {};
        // $scope.destinationCoord = {};
        //$scope.destinationCoord = {};
        if($rootScope.currentBookingObject){
            $scope.destinationCoord.lat= $rootScope.currentBookingObject.destination_point_lat;

            $scope.destinationCoord.long= $rootScope.currentBookingObject.destination_point_long;
        }

      $scope.sendXmppCoord = function (jid) {
          console.log("in sendXmppCoord");
          var posOptions = {timeout: 10000, enableHighAccuracy: false};
          $cordovaGeolocation
              .getCurrentPosition(posOptions)
              .then(function (position) {
                  var lat = position.coords.latitude;
                  var long = position.coords.longitude;
                  //$scope.car = CurrentUser.car().id;
                  $scope.pos = {};
                  $scope.pos.latitude = lat;//$scope.tempLat + $scope.counter;
                  $scope.pos.longitude = long;//$scope.tempLong + $scope.counter;


                  //set variable for map
                  $scope.originCoord.lat= lat;
                  $scope.originCoord.long= long;

                  //  Driver.saveCurrentPosition($scope.pos, 'available');console.log(JSON.stringify($scope.pos));
                  XmppAuth.send_message(jid, JSON.stringify($scope.pos));
                  console.log("out sendXmppCoord", $scope.originCoord);
              }, function (err) {
                  // error
              });
      };
      //*********test function start****************
      $scope.sendMessage = function () {
        if($rootScope.passengerInfo)
        {
            console.log($rootScope.passengerInfo);
            var jid = $rootScope.passengerInfo;
            console.log("jid", jid);
            // XmppAuth.send_message(jid, "message from driver");
            $scope.sendXmppCoord(jid);
        }
        else {
          console.log("rootscope passenger info not found");
        }
      }
      console.log("before send message function");
      $rootScope.sendContPositionToPassenger =  $interval($scope.sendMessage, 3000);
      console.log("after send message function");





        $scope.cancelBooking = function () {

            if($rootScope.currentBookingObject) {

                console.log($rootScope.currentBookingObject);

                //duplicate code start
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Warning',
                    template: 'Are you want to cancel this ride?'
                });
                confirmPopup.then(function(res) {
                    if(res) {
                        console.log('You are sure');

                        //console.log($rootScope.currentBookingObject);
                        // if($rootScope.currentBookingObject){
                        Booking.cancelByDriver({bookingId: $rootScope.currentBookingObject.id})
                            .success(function (response) {
                                console.log(response);



                                if($rootScope.passengerInfo)
                                {
                                    var jid = $rootScope.passengerInfo;

                                    XmppAuth.send_message(jid, "cancelByDriver");

                                }
                                else {
                                    console.log("rootscope driver info not found");
                                }



                                if($rootScope.sendContPositionToPassenger){
                                    $interval.cancel($rootScope.sendContPositionToPassenger);
                                }
                                $state.go('driver_app.driver_home');

                            }).error(function(err){

                            });
                        //}
                    } else {
                        console.log('You are not sure');
                    }
                });
                //duplicate code end


                //*********cancelBooking function end  ****************


            }
            else{
                console.log("rootscope not found");
            }
        };
        //*********cancelBooking function end  ****************

        $scope.reachDestination = function () {

            if($rootScope.currentBookingObject) {

                console.log($rootScope.currentBookingObject);

                //duplicate code start
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Warning',
                    template: 'Are you reach destination?'
                });
                confirmPopup.then(function(res) {
                    if(res) {
                        console.log('You are sure');

                        //console.log($rootScope.currentBookingObject);
                        // if($rootScope.currentBookingObject){
                        Booking.completeByDriver({bookingId: $rootScope.currentBookingObject.id})
                            .success(function (response) {
                                console.log(response);



                                if($rootScope.passengerInfo)
                                {
                                    var jid = $rootScope.passengerInfo;

                                    XmppAuth.send_message(jid, "cancelByDriver");

                                }
                                else {
                                    console.log("rootscope driver info not found");
                                }



                                if($rootScope.sendContPositionToPassenger){
                                    $interval.cancel($rootScope.sendContPositionToPassenger);
                                }
                                $state.go('driver_app.driver_home');

                            }).error(function(err){

                            });
                        //}
                    } else {
                        console.log('You are not sure');
                    }
                });
                //duplicate code end


                //*********cancelBooking function end  ****************


            }
            else{
                console.log("rootscope not found");
            }
        };
    });
