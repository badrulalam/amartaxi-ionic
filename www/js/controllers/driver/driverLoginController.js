/**
 * Created by rbn on 5/14/15.
 */
angular.module('starter')
    .controller('DriverLoginController', function ($scope, $stateParams, DriverAuth, $state, XmppAuth, $ionicPopup,$rootScope) {
        //console.log("driver login");
        $scope.driver = {};
        $scope.driverLogin = function (cred) {
        $rootScope.ionicLoadingShow();
            DriverAuth.login(cred).success(function (result) {
                $rootScope.ionicLoadingHide();
                $state.go('driver_app.driver_home');
            }).error(function (err) {
                $rootScope.ionicLoadingHide();
                //$scope.showAlert(err.err);
                $rootScope.showAlert("Error", err.err);
            });
        };
        //$scope.showAlert = function(message) {
        //    var alertPopup = $ionicPopup.alert({
        //        title: 'Error!',
        //        template: message
        //    });
        //    alertPopup.then(function(res) {
        //        console.log('after alertPopup');
        //    });
        //};
    });
