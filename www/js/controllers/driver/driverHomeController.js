/**
 * Created by rbn on 5/14/15.
 */
angular.module('starter')
    // .controller('DriverHomeController', function($scope, $stateParams) {
    //     console.log("Driver Home");
    // });
    .controller('DriverHomeController', function ( $scope, $stateParams,$rootScope, $state, XmppAuth, $cordovaGeolocation, $interval, CurrentUser, Driver, LocalService,$ionicActionSheet,$timeout, Booking, $ionicPopup,$ionicHistory) {
        $scope.driver_status = "";
        try{
            $scope.driver_status = CurrentUser.car().status;
        }catch(e){

        }
        $scope.reload = function(){
            //$route.reload();
        }
        if (!LocalService.get('auth_token')) {

            $state.go('driver_login');
            // console.loCurrentUser.car()
        }

        $ionicHistory.clearHistory();

        $scope.ash = "ashraf";
        //xmpp+strophe start
        $scope.login = {};
        $scope.chat = {};
        // XmppAuth.connect_me_now("driver@amartaxi.com","driver");
        // XmppAuth.connect_me_now("tanvir@sunnahnikaah.com","1234");

        $scope.XConnect = function () {
            console.log($scope.login);
            XmppAuth.connect_me_now($scope.login.username, $scope.login.password);

        };
        $scope.XSend = function () {
            console.log($scope.login);
            XmppAuth.send_message($scope.chat.jid, $scope.chat.message);

        };
        $scope.hitCarPosition = function () {
            $scope.isRunning = true;
            scroll = $interval($scope.getMyPosition, 1000);

        };
        $scope.stopCarPosition = function () {
            $scope.isRunning = false;
            $interval.cancel(scroll);
        };
        $scope.getMyPosition = function () {
            console.log("i am in get My position");
            var posOptions = {timeout: 10000, enableHighAccuracy: false};
            $cordovaGeolocation
                .getCurrentPosition(posOptions)
                  .then(function (position) {
                      var lat = position.coords.latitude;
                      var long = position.coords.longitude;
                      $scope.car = CurrentUser.car().id; console.log(CurrentUser.car());
                      $scope.pos = {};
                      $scope.pos.latitude = lat;
                      $scope.pos.longitude = long;
                      console.log("$scope.pos", $scope.pos);
                      console.log("$scope.car", $scope.car);

                    console.log("getmyposition lat long: ",lat+" "+long);

                      Driver.savePosition($scope.pos, $scope.car);
                    console.log("Before Calling savecurrent Position",JSON.stringify($scope.pos));
                    Driver.saveCurrentPosition($scope.pos);//console.log(JSON.stringify($scope.pos));
                    console.log("After Calling savecurrent Position");
                  }, function (err) {
                      // error
                    console.log("error: ",err);
                  });
          };
          // $scope.sendXmppCoord = function () {
          //     console.log("in sendXmppCoord");
          //     var posOptions = {timeout: 10000, enableHighAccuracy: false};
          //     $cordovaGeolocation
          //         .getCurrentPosition(posOptions)
          //         .then(function (position) {
          //             var lat = position.coords.latitude;
          //             var long = position.coords.longitude;
          //             //$scope.car = CurrentUser.car().id;
          //             $scope.pos = {};
          //             $scope.pos.latitude = lat;//$scope.tempLat + $scope.counter;
          //             $scope.pos.longitude = long;//$scope.tempLong + $scope.counter;
          //             //  Driver.saveCurrentPosition($scope.pos, 'available');console.log(JSON.stringify($scope.pos));
          //           //  XmppAuth.send_message("passenger@amartaxi.com", JSON.stringify($scope.pos));
          //             console.log("out sendXmppCoord");
          //         }, function (err) {
          //             // error
          //         });
          // };
          $scope.sendFreqCoord = function () {
              scroll = $interval($scope.sendXmppCoord, 3000);
          };
          $scope.Connect = function () {

              XmppAuth.connect_me_now(username, password);

              $('#password').val('');

              $('.chat-login-form').hide();
              $('.chat-logout-form').show();
          };

          $scope.DisConnect = function () {
              XmppAuth.disconnect_me();
              $('.chat-login-form').show();
              $('.chat-logout-form').hide();
          };

          $scope.collapse_me = function (boxId) {
              console.log(boxId, 'collapse me');
              if ($('#' + boxId).css("height") == "38px") {
                  $('#' + boxId).css("height", "300px");
                  $('#' + boxId).css("margin-top", "-250px");
              } else {
                  $('#' + boxId).css("height", "38px");
                  $('#' + boxId).css("margin-top", "0px");
              }
          };

          $scope.hide_me = function (boxId) {
              console.log(boxId, 'hide me');
              $('#' + boxId).hide();
          };

          $scope.add_contact = function () {
              console.log("new-chat");
              $('#contact_dialog').dialog('open');
          };
          $scope.changeMyPresence = function (status) {
              $scope.driver_status = status;
              if (status == "available") {
                  XmppAuth.online();
                  Driver.changePresence(status);

                  if($rootScope.saveContPositoin){
                    $interval.cancel($rootScope.saveContPositoin);
                    $rootScope.saveContPositoin = $interval($scope.getMyPosition, 15000);
                  }
                  else {
                    $rootScope.saveContPositoin = $interval($scope.getMyPosition, 15000);
                  }

              }
              if (status == "busy") {
                  XmppAuth.busy();
                  Driver.changePresence(status);
                //  $rootScope.saveContPositoin = $interval($scope.getMyPosition, 15000);

              }
              if (status == "unavailable") {
                  XmppAuth.offline();
                  Driver.changePresence(status);
                  //$rootScope.saveContPositoin = $interval($scope.getMyPosition, 15000);

                  if($rootScope.saveContPositoin){
                    $interval.cancel($rootScope.saveContPositoin);
                  }


            }
        };
        var x2js = new X2JS();
        $scope.on_message = function (message) {

            try {
                console.log('msg from passenger', message);  //*************************************************
                var msgJson = x2js.xml2json(message);

                console.log('messages',msgJson);
                var msgXml = x2js.json2xml(msgJson);
                console.log('msgXML',JSON.stringify(msgXml));

                //save Received message on rootScope
                $rootScope.passengerInfo = msgJson._from;
                // TODO: assigned to ME (Robin)
                 var bookingObject = JSON.parse(msgJson.body);
                console.log("bookingObject ", bookingObject );
                if(typeof bookingObject !== "string")
                {
                    $rootScope.msgJson = JSON.parse(msgJson.body);

                    $rootScope.currentBookingObject = bookingObject;
                    Booking.isDriverFree(CurrentUser.car().id)
                        .success(function(response){ console.log("mahfuz 2 ", response);
                            if(response.current_booking_id == "" || !response.hasOwnProperty('current_booking_id')){
                                $scope.show(msgJson);
                            }
                        });
                }
            }
            catch(err) {
                console.log('error',err);
            }
            try {
                console.log('msg from passenger', message);  //*************************************************
                var msgJson = x2js.xml2json(message);
                if(msgJson.body == "late_response")
                {
                    $scope.hideSheet();
                    //alert("Sorry another driver selected");
                    $rootScope.showAlert("Alert","Sorry another driver selected")
                }
            }
            catch(err) {
                console.log('error',err);
            }
            try {
                console.log('msg from passenger', message);  //*************************************************
                var msgJson = x2js.xml2json(message);
                if(msgJson.body == "cancelByPassenger")
                {
                    $scope.hideSheet();
                    if($rootScope.sendContPositionToPassenger){
                        $interval.cancel($rootScope.sendContPositionToPassenger);
                    }
                    //alert("This ride cancal by passenger");
                    $rootScope.showAlert("Alert", "This ride cancal by passenger");

                    $state.go("driver_app.driver_home");
                }
            }
            catch(err) {
                console.log('error',err);
            }
            try {
                console.log('msg from passenger', message);  //*************************************************
                var msgJson = x2js.xml2json(message);
                if(msgJson.body == "completeByPassenger")
                {
                    $scope.hideSheet();
                    if($rootScope.sendContPositionToPassenger){
                        $interval.cancel($rootScope.sendContPositionToPassenger);
                    }
                    //var alertPopup = $ionicPopup.alert({
                   //     title: "Message",
                   //     template: "Ride Complete"
                   // });
                    $rootScope.showAlert("Message","Ride Complete");
                   // "This ride cancal by passenger"
                   // alertPopup.then(function(res) {
                    //    console.log('Thank you for not eating my delicious ice cream cone');
                        $state.go("driver_app.driver_home");
                   // });
                }
            }
            catch(err) {
                console.log('error',err);
            }
            try {
                console.log('msg from passenger', message);  //*************************************************
                var msgJson = x2js.xml2json(message);
                if(msgJson.body == "driverReachedByPassenger")
                {
                    //$scope.hideSheet();
                    //if($rootScope.sendContPositionToPassenger){
                    //    $interval.cancel($rootScope.sendContPositionToPassenger);
                    //}
                    //alert("Driver found  by passenger");
                    $rootScope.showAlert("Message", "Driver found  by passenger");
                    if($rootScope.sendContPositionToPassenger){
                        $interval.cancel($rootScope.sendContPositionToPassenger);
                    }
                    $state.go("driver_app.driver_reached_destination");

                }
            }
            catch(err) {
                console.log('error',err);
            }
            return true;
        };
        $(document).bind('connected', function () {

            console.log("limon connected");

            var iq = $iq({type: 'get'}).c('query', {xmlns: 'jabber:iq:roster'});
            console.log(iq.toString());
            XmppAuth.connection.sendIQ(iq, XmppAuth.on_roster);
            XmppAuth.connection.addHandler(XmppAuth.on_roster_changed, "jabber:iq:roster", "iq", "set");
            XmppAuth.connection.addHandler($scope.on_message, null, "message", "chat");
        });


        //Actionbar
        $scope.show = function (msg){
            bodyJson = JSON.parse(msg.body);
            // Show the action sheet
            $scope.hideSheet = $ionicActionSheet.show({
                buttons: [
                    {text: 'Accept'},
                    {text: 'Decline'}
                ],
                //destructiveText: 'Delete',
                titleText: 'Pick Up Point: ' + bodyJson.pickup_point_name+'\nDestination Point: '+bodyJson.destination_point_name,
                cancelText: 'Ignore',
                cancel: function () {
                    // add cancel code..
                },
                buttonClicked: function (index) {
                    if (index === 0) {
                        console.log("Accepted- Redirecting to ");
                        XmppAuth.send_message(msg._from, 'Accept');

                        $state.go('driver_app.driver_message_received');
                        //$rootScope.sendContPositionToPassengerFunction();
                    }
                    else if (index === 1) {
                      XmppAuth.send_message(msg._from, 'Decline');
                        //alert("Declined");
                    }
                    return true;
                }
            });

            // For example's sake, hide the sheet after two seconds
            $timeout(function () {
                $scope.hideSheet();
            }, 100000);
        };

       //start a cont call back function
       // if($rootScope.saveContPositoin){
       //   $interval.cancel($rootScope.saveContPositoin);
       //   $rootScope.saveContPositoin = $interval($scope.getMyPosition, 15000);
       // }
       // else {
       //   $rootScope.saveContPositoin = $interval($scope.getMyPosition, 15000);
       // }

$scope.data = {};
        $scope.testF = function () {
            var myPopup = $ionicPopup.show({
                template: '<rating ng-model="data.rating" max=5 readonly=false  on-leave="overStar = null"></rating>',
                title: 'Rate Driver',
                //subTitle: 'Please use normal things',
                scope: $scope,
                buttons: [
                    { text: 'Cancel' },
                    {
                        text: '<b>Save</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                            if (!$scope.data.rating) {
                                //don't allow the user to close unless he enters wifi password
                                e.preventDefault();
                            } else {
                                console.log($scope.data.rating);
                                return $scope.data.rating;
                            }
                        }
                    }
                ]
            });
            console.log("test");
        };
        console.log("sendContPositionToPassenger");

    });
