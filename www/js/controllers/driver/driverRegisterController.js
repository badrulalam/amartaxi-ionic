/**
 * Created by rbn on 5/14/15.
 */
angular.module('starter')

    .controller('DriverRegisterController', function($scope, $stateParams,DriverAuth,$state) {
        console.log("driver Register");

        $scope.submitForm = function(isValid) {
            // check to make sure the form is completely valid
            if (isValid) {
                 console.log('our form is amazing');
            }
        };

        $scope.driverRegister = function(cred){
            console.log(cred.email);
            console.log(cred);
            DriverAuth.register(cred).success(function (result) {
                console.log("register succeded");
                $state.go('driver_app.driver_home');
            }).error(function (err) {
                console.log("registration failed");
                // $scope.showToast('Login failed, Please Try Again', 'short','center');
            });
        }
    });
