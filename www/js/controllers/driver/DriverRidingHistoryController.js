/**
 * Created by rbn on 5/14/15.
 */
angular.module('starter')
    .controller('DriverRidingHistoryController', function($scope,$state, $http, CurrentUser, Booking, $ionicLoading) {

        Booking.getByParamsDriver( CurrentUser.car().id)
            .success(function(response){
                console.log(response);
                $scope.rides = response.bookings;
                $scope.hide();
            });

        $scope.show = function() {
            $ionicLoading.show({
                template:  ' <div class="spinner"> <div class="cube1"></div> <div class="cube2"></div> </div>'
            });
        };
        $scope.hide = function(){
            $ionicLoading.hide();
        };
        $scope.show();
    });
