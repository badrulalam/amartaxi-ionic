angular.module('starter')
    .controller('SettingsController', function($scope,$translate,$ionicActionSheet,$timeout) {
        console.log("settings controller");
        //$state.go('app.profile');
        console.log($translate.use());
        $scope.currentLang = "";


        $scope.checkLang = function(){
            if($translate.use()==="en_EN"){
                $scope.currentLang = "English"
            }
            else if($translate.use()==="bn_BN"){
                $scope.currentLang = "বাংলা";
            }
        };
        $scope.checkLang();


        $scope.show = function() {

            // Show the action sheet
            var hideSheet = $ionicActionSheet.show({
                buttons: [
                    { text: 'বাংলা' },
                    { text: 'ENGLISH' }
                ],
                //destructiveText: 'Delete',
                //titleText: 'Modify your album',
                cancelText: 'Cancel',
                cancel: function() {
                    // add cancel code..
                },
                buttonClicked: function(index) {
                    if(index === 0){
                        console.log("bn");
                        $scope.changeLanguage('bn_BN');
                        $scope.checkLang();
                    }
                    else if(index === 1){
                        console.log("en");
                        $scope.changeLanguage('en_EN');
                        $scope.checkLang();
                    }
                    return true;
                }
            });

            // For example's sake, hide the sheet after two seconds
            $timeout(function() {
                hideSheet();
            }, 10000);

        };
    });