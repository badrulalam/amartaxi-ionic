angular.module('starter')
    .controller('ProfileController', function($scope,CurrentUser,Auth) {
        console.log("Profile controller");

        $scope.user = {
            name: CurrentUser.user().name,
            mobile: CurrentUser.user().mobile,
            email: CurrentUser.user().email,
            id: CurrentUser.user().id
            //encryptedPassword: '12345'
        };

        $scope.updateProfile = function(){
            console.log($scope.user);
            Auth.update($scope.user).success(function(response){
                console.log('response from server',response);
            }).error(function(){
                console.log('error response');
            });
        };

    });
