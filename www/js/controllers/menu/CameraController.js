/**
 * Created by mahfuz on 11/4/15.
 */
angular.module('starter')
    .controller('CameraController', function($scope, $cordovaCamera) {
        console.log("CameraController controller");

        $scope.pictureUrl = "http://placehold.it/340x340;"

        $scope.takePicture = function(){
            var option = {
                destinationType: Camera.DestinationType.DATA_URL,
                encodingType: Camera.EncodingType.JPEG
            };
            $cordovaCamera.getPicture(option)
                .then(function(data){
                    console.log("camera data", angular.toJson(data));
                    $scope.pictureUrl = 'data:image/jpeg;base64,' + data;
                }, function(err){
                    console.log("camera err", angular.toJson(err));
                });
        };

    });