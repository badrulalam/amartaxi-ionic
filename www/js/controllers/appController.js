angular.module('starter.controllers', [])
    .controller('AppController', function($scope,$window,  $timeout,Auth,$ionicModal, $state,XmppAuth, $rootScope, $interval, $rootScope,$ionicPopup, CurrentUser, $cordovaGeolocation, $ionicPlatform, LatlngToLocationName, LocalService, $http, Booking,$cordovaToast,$ionicHistory
    ) {
        // Form data for the login modal

        //$scope.loginData = {};
        $scope.isActiveMapInteraction = {};
        $scope.isActiveMapInteraction.pickup = false;
        $scope.isActiveMapInteraction.destination = false;
        $scope.isActiveMapInteraction.showMarker = true;
        var marker, map;
        // Create the login modal that we will use later


        // Perform the login action when the user submits the login form


        $scope.logout = function(){
            if($rootScope.saveContPositoin){
                $interval.cancel($rootScope.saveContPositoin);
            }
            if($rootScope.sendContPositionToPassenger){
                $interval.cancel($rootScope.sendContPositionToPassenger);
            }
            if(XmppAuth.connection){
                XmppAuth.disconnect_me();
            }

            Auth.logout();
            $state.go('passenger_login');
        };
        $scope.goToHome = function(){
            $state.go('app.passenger_home');
        };
        $scope.goToRideHistory = function(){
            $state.go('app.passenger_riding_history');
        };


    //    ash changes ////////////////////////////////////

        console.log("in home controller");
        if (!LocalService.get('auth_token')) {
            $state.go('passenger_login');
        }
        $ionicHistory.clearHistory();
        $scope.map = {};
        $scope.map.center = {};
        $scope.map.zoom = 18;
        //$scope.map.center.latitude = 23.7494231;
        //$scope.map.center.longitude = 90.3830754;
        $scope.map = {center: {latitude: 23.7494231, longitude: 90.3830754}, zoom: 18};
        geocoder = new google.maps.Geocoder();
      //  var marker, map;
        $rootScope.params = {};

        $rootScope.params.selectedPickUpPoint = {latitude: '', longitude: '', locname: ''};
        $rootScope.params.selectedDestinationPoint = {latitude: '', longitude: '', locname: ''};

        $rootScope.params.driverLocationPoint = {latitude: '', longitude: ''};


        $rootScope.params.rideType = "yellow";
        $rootScope.params.driverPositions= [];
        $rootScope.params.directionStatus = '';
        $scope.destinationpoint = "Select Destination";
        $rootScope.updatePickup = function (lat, long, name) {
            $rootScope.params.selectedPickUpPoint = {latitude: lat, longitude: long, locname: name};
            var latlng = new google.maps.LatLng(lat, long);
            marker.setPosition(latlng);
            console.log('selectedPickupPoint', $rootScope.params.selectedPickUpPoint);
        };

        $rootScope.updateDestination = function (lat, long, name) {
            $rootScope.params.selectedDestinationPoint = {latitude: lat, longitude: long, locname: name};
            $scope.destinationpoint = name;
            var latlng = new google.maps.LatLng(lat, long);
            marker.setPosition(latlng);
                console.log('selectedDestination', $rootScope.params.selectedDestinationPoint);
        };

        $scope.updatePickupLoc = function (lat, long, name) {
            $rootScope.params.selectedPickUpPoint = {latitude: lat, longitude: long, locname: name};
            $rootScope.params.selectedPickUpPoint.latitude  = lat;
            $rootScope.params.selectedPickUpPoint.longitude = long;
            $rootScope.params.selectedPickUpPoint.locname   = name; $scope.$apply();
            var latlng = new google.maps.LatLng(lat, long);
            //marker.setPosition(latlng);
            console.log('selectedPickupPoint', $rootScope.params.selectedPickUpPoint);

            $scope.$broadcast ('sentPickupLocation', $rootScope.params.selectedPickUpPoint);

        };
        $scope.updateDestinationLoc = function (lat, long, name) {
            $rootScope.params.selectedDestinationPoint = {latitude: lat, longitude: long, locname: name};$scope.$apply();
            var latlng = new google.maps.LatLng(lat, long);
            //marker.setPosition(latlng);
            $scope.destinationpoint = name;

            console.log('selectedDestinationPoint', $rootScope.params.selectedDestinationPoint);
            $scope.$broadcast ('sentDestinationLocation', $rootScope.params.selectedDestinationPoint);

        };

        var delay = (function () {
            var timer = 1;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();
        //$scope.centerChanged = function(){
        //    delay(function(){
        //        //console.log($scope.map.center.longitude);
        //        alert(center);
        //    }, 500);
        //}
        $scope.$on('mapInitialized', function (evt, evtMap) {
            map = evtMap;
            marker = map.markers[0];
            $scope.getMyPosition();
        });
        $scope.positions = [{lat:37.7699298,lng:-122.4469157}];
        $scope.addMarker = function(event) {
            var ll = event.latLng;
            $scope.positions.push({lat:ll.lat(), lng: ll.lng()});
        }
        $scope.centerChanged = function (event) {
            if($scope.isActiveMapInteraction.pickup) {
                marker.setPosition(map.getCenter());
            }
            if($scope.isActiveMapInteraction.destination){
                marker.setPosition(map.getCenter());
            }
        };

        $scope.click = function (event) {
            map.setZoom(18);
            map.setCenter(marker.getPosition());
        };

        $scope.markerPostitionChanged = function (event) {
            delay(function () {
                map.panTo(marker.getPosition());
                console.log("marker changed");

                var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
                //console.log(marker.position.lat());
                geocoder.geocode({'latLng': latlng}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            console.log(results[1].formatted_address);
                            $scope.pickuppoint = results[1].formatted_address;
                            //marker.setPosition(latlng);
                            if($scope.isActiveMapInteraction.pickup){
                                $scope.updatePickupLoc(marker.position.lat(), marker.position.lng(), $scope.pickuppoint);
                            }
                            if($scope.isActiveMapInteraction.destination){
                                $scope.updateDestinationLoc(marker.position.lat(), marker.position.lng(), $scope.pickuppoint);
                            }


                        } else {
                            console.log('No results found');
                        }
                    } else {
                        console.log('Geocoder failed due to: ' + status);
                    }
                });
                console.log('pickup', $rootScope.params.selectedPickUpPoint);
                console.log('destination',$rootScope.params.selectedDestinationPoint );
            }, 500);
        };
        $rootScope.myCurrentLocation = {};
        $scope.getMyPosition = function () {   //$rootScope.showAlert("Message", "this is test message");
            var posOptions = {timeout: 10000, enableHighAccuracy: false};
            $cordovaGeolocation
                .getCurrentPosition(posOptions)
                .then(function (position) {
                    var lat = position.coords.latitude;
                    var long = position.coords.longitude;
                    $rootScope.myCurrentLocation.lat = lat;
                    $rootScope.myCurrentLocation.lng = long;
                    $scope.latitude = lat;
                    $scope.longitude = long;
                    $scope.map = {center: {latitude: $scope.latitude, longitude: $scope.longitude}, zoom: 18};
                    console.log("lat: " + lat + " long: " + long);
                    var latlng = new google.maps.LatLng($scope.latitude, $scope.longitude);

                    geocoder.geocode({'latLng': latlng}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[1]) {
                                console.log("addreses", results);
                                $scope.pickuppoint = results[1].formatted_address;

                                console.log("asdasdasdasd");
                                console.log("map.panTo(latlng); commented");
                                map.panTo(latlng);
                                $rootScope.updatePickup($scope.latitude, $scope.longitude, $scope.pickuppoint);
                            } else {
                                console.log('No results found');
                            }
                        } else {
                            console.log('Geocoder failed due to: ' + status);
                        }
                    });
                }, function (err) {
                    console.log(err);
                });
        };


        $scope.changeMapCenter = function () {
            //to nothing
        };

        $scope.options = {scrollwheel: true};
        $scope.coordsUpdates = 0;
        $scope.dynamicMoveCtr = 0;
        $scope.marker = {
            id: 0,
            coords: {
                latitude: $scope.map.center.latitude,
                longitude: $scope.map.center.longitude
            },
            options: {draggable: false},
            events: {
                dragend: function (marker, eventName, args) {
                    $log.log('marker dragend');
                    var lat = marker.getPosition().lat();
                    var lon = marker.getPosition().lng();
                    $log.log(lat);
                    $log.log(lon);

                    $scope.marker.options = {
                        draggable: true,
                        labelContent: "lat: " + $scope.marker.coords.latitude + ' ' + 'lon: ' + $scope.marker.coords.longitude,
                        labelAnchor: "100 0",
                        labelClass: "marker-labels"
                    };
                }
            }
        };

$scope.myBack = function(){
    //$ionicHistory.clearHistory();
    //$ionicHistory.clearCache();
    //$ionicHistory.goBack();
    //$ionicHistory.clearHistory();
    //$ionicHistory.clearCache();
    $state.go('app.passenger_home');
    $ionicHistory.nextViewOptions({
        //disableAnimate: true,
        disableBack: true
    });
};
        $scope.hideDriverMarkers = function() {

            $rootScope.params.driverPositions = [];

        };
        $scope.showDriverMarkers = function() {
            //for (var key in $rootScope.params.driverPositions) {
            //    map.markers[key].setMap(map);
            //};
        };

        $scope.hideMyMarker = function() {
            marker.setMap(null);
        };
        $scope.showMyMarker = function() {
            marker.setMap(map);
        };
        $rootScope.mapReset = function() { console.log('in map reset');
            $rootScope.goToHomeReset();
            $scope.showMyMarker();
            $rootScope.restLocationLabel();
            $rootScope.params.driverPositions= [];
            $rootScope.params.selectedPickUpPoint = {latitude: '', longitude: '', locname: ''};
            $rootScope.params.selectedDestinationPoint = {latitude: '', longitude: '', locname: ''};

            $rootScope.params.directionStatus = '';

        };
        $rootScope.goToHomeResetViaFindDriver = function() { console.log('in map reset via find driver');
            $rootScope.goToHomeReset();
            $scope.showMyMarker();
            $rootScope.params.driverPositions= [];
            $rootScope.params.directionStatus = '';

        };
        $rootScope.goToHomeReset = function(){
            $scope.isActiveMapInteraction.pickup = false;
            $scope.isActiveMapInteraction.destination = false;
            $scope.isActiveMapInteraction.showMarker = false;

            $ionicHistory.nextViewOptions({
                //disableAnimate: true,
                disableBack: true
            });
        };
        $scope.catchMeIfYouCan = function()
        {
            console.log("OK, You catch me!!");
        };
        $scope.$on("catchYou", $scope.catchMeIfYouCan);
        //$timeout(function() {
        //    $scope.getMyPosition();
        //}, 5000);
        $rootScope.setMarkerOnPickup = function(){
            if($rootScope.params.selectedPickUpPoint.latitude == '' &&  $rootScope.params.selectedPickUpPoint.longitude == ''){
                $rootScope.params.selectedPickUpPoint.latitude = 23.791633056390143;
                $rootScope.params.selectedPickUpPoint.longitude = 90.40751290851185;
                $rootScope.params.selectedPickUpPoint.locname = "বনানী, ঢাকা, বাংলাদেশ";

            }
            var latlng = new google.maps.LatLng($rootScope.params.selectedPickUpPoint.latitude, $rootScope.params.selectedPickUpPoint.longitude);
            marker.setPosition(latlng);
        };
        $rootScope.setMarkerOnDestination = function(){
            if($rootScope.params.selectedDestinationPoint.latitude == '' &&  $rootScope.params.selectedDestinationPoint.longitude == ''){
                $rootScope.params.selectedDestinationPoint.latitude = 23.791633056390143;
                $rootScope.params.selectedDestinationPoint.longitude = 90.40751290851185;
                $rootScope.params.selectedDestinationPoint.locname = "বনানী, ঢাকা, বাংলাদেশ";

            }
            var latlng = new google.maps.LatLng($rootScope.params.selectedDestinationPoint.latitude, $rootScope.params.selectedDestinationPoint.longitude);
            marker.setPosition(latlng);
        };






        $ionicModal.fromTemplateUrl('templates/menu/settings/contact-support.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.ContactSupportModal = modal;
        });
        $scope.openContactSupportModal = function(){
            $scope.ContactSupportModal.show();
        };
        $scope.closeContactSupportModal = function(){
            $scope.ContactSupportModal.hide();
        };
        $scope.doContactSupportModal = function() {
            $timeout(function() {
                $scope.closeContactSupportModal();
            }, 500);
        };

        $ionicModal.fromTemplateUrl('templates/passenger/current_booking_modal.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.PassengerCurrentBookingModal = modal;
        });
        $scope.openPassengerCurrentBookingModal = function(){
            $scope.PassengerCurrentBookingModal.show();
            $scope.getPassengerCurrentBooking();
        };
        $scope.closePassengerCurrentBookingModal = function(){
            $scope.PassengerCurrentBookingModal.hide();
            $scope.current_booking =  null;
        };
        $scope.doPassengerCurrentBookingModal = function() {
            $timeout(function() {
                $scope.closePassengerCurrentBookingModal();
            }, 200);
        };
        $scope.getPassengerCurrentBooking  = function(){
            $rootScope.ionicLoadingShow();
            Booking.getCurrentBooking( CurrentUser.user().id)
                .success(function(response){

                    if(response.current_booking_id == ""){
                        $scope.current_booking = null;
                    }else{
                       // $scope.current_booking = response.current_booking_id;
                        Booking.get(response.current_booking_id)
                            .success(function(data){
                                $scope.current_booking = data;
                            });

                    }
                    $rootScope.ionicLoadingHide();
                });
        };

        $ionicModal.fromTemplateUrl('templates/passenger/riding_history.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.PassengerRidingHistoryModal = modal;
        });
        $scope.openPassengerRidingHistoryModal = function(){
            $scope.PassengerRidingHistoryModal.show();
            $scope.getPassengerRidingHistory();
        };
        $scope.closePassengerRidingHistoryModal = function(){
            $scope.PassengerRidingHistoryModal.hide();
            $scope.rides =  [];
        };
        $scope.doPassengerRidingHistoryModal = function() {
            $timeout(function() {
                $scope.closePassengerRidingHistoryModal();
            }, 200);
        };
        $scope.getPassengerRidingHistory  = function(){
            $rootScope.ionicLoadingShow();
            Booking.getRideList( CurrentUser.user().id)
                .success(function(response){
                    console.log(response);
                    $scope.rides = response.bookings;
                    $rootScope.ionicLoadingHide();
                });
        };


        $ionicModal.fromTemplateUrl('templates/passenger/riding_details.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.PassengerRidingDetailsModal = modal;
        });
        $scope.openPassengerRidingDetailsModal = function(id){
            $scope.PassengerRidingDetailsModal.show();
            $scope.getPassengerRidingDetails(id);
        };
        $scope.closePassengerRidingDetailsModal = function(){
            $scope.PassengerRidingDetailsModal.hide();
            $scope.booking = null;
        };
        $scope.doPassengerRidingDetailsModal = function() {
            $timeout(function() {
                $scope.closePassengerRidingDetailsModal();
            }, 200);
        };
        $scope.getPassengerRidingDetails  = function(id){
            $rootScope.ionicLoadingShow();
            Booking.get(id)
                .success(function(response){
                    $scope.booking = response;
                    $rootScope.ionicLoadingHide();
                });
        };
        $scope.closePassengerAllRidingModal = function(){
            $scope.closePassengerRidingDetailsModal();
            $scope.closePassengerRidingHistoryModal();

        };
        $scope.fixed_icon = {
            "top": "118px",
            "left": $window.innerWidth/2 -16 + "px",
            "z-index": "10",
            "position": "absolute"
        };



        $ionicModal.fromTemplateUrl('templates/menu/passenger/setting.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.SettingModal = modal;
        });
        $scope.openSettingModal = function(){
            $scope.SettingModal.show();
        };
        $scope.closeSettingModal = function(){
            $scope.SettingModal.hide();
        };
        $scope.doSettingModal = function() {
            $timeout(function() {
                $scope.closeSettingModal();
            }, 500);
        };
        $rootScope.restLocationLabel = function()
        {
            $scope.pickuppoint = "Select Pickup Point";
            $scope.destinationpoint = "Select Destination Point";
        }

    });
