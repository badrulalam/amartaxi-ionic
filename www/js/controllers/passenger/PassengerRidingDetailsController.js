/**
 * Created by rbn on 5/14/15.
 */
angular.module('starter')
    .controller('PassengerRidingDetailsController', function($scope,$state,$stateParams, $http, Booking, $ionicPopup) {

        $scope.data = {};
        $scope.rateDriver = function(){


            //alert();
            var myPopup = $ionicPopup.show({
                template: '<rating ng-model="data.rating" max=5 readonly=false  on-leave="overStar = null"></rating>',
                title: 'Rate Driver',
                //subTitle: 'Please use normal things',
                scope: $scope,
                buttons: [
                    { text: 'Cancel'},
                    {
                        text: '<b>Save</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                            if (!$scope.data.rating) {
                                //don't allow the user to close unless he enters wifi password
                                e.preventDefault();
                            } else {
                                //console.log($scope.data.rating);
                                //return $scope.data.rating;


                                Booking.rateBooking({bookingId: $scope.booking.id, rating: $scope.data.rating})
                                    .success(function (response) {
                                        $scope.booking.rating = response.rating;
                                        $scope.data.rating = null;

                                    }).error(function(err){
                                        console.log("after rating response error", err);

                                    });
                            }
                        }
                    }
                ]
            });
        };
        $scope.cancelRide = function(){
          //alert();



            var confirmPopup = $ionicPopup.confirm({
                    title: 'Alert',
                    template: 'Are you sure you want to complete this ride?'
                });
                confirmPopup.then(function(res) {
                    if(res) {
                        console.log('You are sure');
                        Booking.cancelByPassenger({bookingId: $scope.booking.id})
                            .success(function (response) {

                                $scope.booking.is_active = response.is_active;
                                $scope.booking.is_cancelled = response.is_cancelled ;
                                $scope.booking.cancelBy = response.cancelBy ;
                                $scope.booking.is_complete = response.is_complete ;
                                $scope.booking.completeBy = response.completeBy ;
                            }).error(function(err){

                            });
                    } else {
                        console.log('You are not sure');
                    }
                });
        };
        $scope.completeRide = function(){
          //alert();



            var confirmPopup = $ionicPopup.confirm({
                    title: 'Alert',
                    template: 'Are you sure you want to complete this ride?'
                });
                confirmPopup.then(function(res) {
                    if(res) {
                        console.log('You are sure');
                        Booking.completeByPassenger({bookingId: $scope.booking.id})
                            .success(function (response) {
                                $scope.booking.is_active = response.is_active;
                                $scope.booking.is_cancelled = response.is_cancelled ;
                                $scope.booking.cancelBy = response.cancelBy ;
                                $scope.booking.is_complete = response.is_complete ;
                                $scope.booking.completeBy = response.completeBy ;

                            }).error(function(err){

                            });
                    } else {
                        console.log('You are not sure');
                    }
                });
        };


    });
