/**
 * Created by rbn on 5/14/15.
 */
angular.module('starter')

    .controller('PassengerContactingDriverController', function ($scope, $stateParams, $state, $interval, $rootScope, Driver, $ionicBackdrop, $timeout, $ionicLoading, $ionicHistory, XmppAuth, Booking, $ionicPopup) {
        //XmppAuth.connection.addHandler($scope.on_message, null, "message", "chat");
        $scope.show = function () {
            $ionicLoading.show({
                template: '<p class="loader-waiting">Waiting for driver response...</p> <div class="spinner"> <div class="cube1"></div> <div class="cube2"></div> </div>'
            });
        };

        $scope.hide = function () {
            $ionicLoading.hide();
        };
        $scope.confirmDriver = function (driver) {
            Booking.selectedDriver({bookingId: $rootScope.currentBookingObject.id, driverId: driver.id})
                .success(function (response) {
                    console.log("emergent", response);
                    console.log('selected Driver', driver);
                    $rootScope.driverInfo = driver;
                    console.log(driver);
                    $state.go('app.passenger_track_driver');

                }).error(function (err) {
                    console.log("emergent", err);
                });
        };
        $scope.cancelRide = function (value) {



            if ($rootScope.currentBookingObject) {
                if (Booking.cancelRideByPassenger($rootScope.currentBookingObject)) {
                    $scope.$parent.mapReset();
                    $state.go('app.passenger_home');
                }

            }


        },
            //$scope.show();
            //$timeout(function() {
            //    $scope.hide();
            //}, 5000);

            $scope.passengerLoader = true;
        // $timeout(function() {
        //     $scope.passengerLoader = false;
        // }, 3000);

        $scope.allDrivers = {};
        $scope.doRefresh = function () {
            Driver.available().success(function (result) {
                //console.log("result info"+result);
                $scope.allDrivers = result;
            });
        };
        Driver.available().success(function (result) {
            console.log(result);
            $scope.allDrivers = result;
        });
        var x2js = new X2JS();
        $scope.on_message = function (message) {
            try {
                console.log('accept reject reply', message);  //*******************
                var msgJson = x2js.xml2json(message);
                var jid = msgJson._from;
                var jname = XmppAuth.jid_to_node(jid);
                console.log(msgJson);
                var status = msgJson.body;
                if (status == "Accept") {
                    $("#" + jname).css("background-color", "green");

                    $scope.cancelLateDriverResponse(jname);

                    console.log($scope.allDrivers);
                }
                if (status == "Decline") {
                    $("#" + jname).css("background-color", "red");
                }
                if (status == "cancelByDriver") {
                    $rootScope.showAlert("Message", "This ride cancal by driver");
                    $interval.cancel($rootScope.changeDriverInterval);
                    $interval.cancel($rootScope.changeDestinationPointInterval);

                    $scope.$parent.mapReset();
                    $state.go("app.passenger_home");
                }
                if (status == "passengerFoundByDriver") {
                    $interval.cancel($rootScope.changeDriverInterval);

                    $rootScope.showAlert("Message", "This driver reached");
                    $state.go("app.passenger_reached_destination");
                }

            } catch (err) {
                console.log('error', err);
            }
            return true;
        };
        $scope.cancelLateDriverResponse = function (driver_jid_name) {
            console.log(driver_jid_name);
            var selected_driver = {};
            $scope.allDrivers.forEach(function (driver) {
                if (driver.jid != driver_jid_name) {

                    var message = "late_response";

                    XmppAuth.send_message(driver.jid + "@amartaxi.com", message);// JSON.stringify(message));
                    console.log(driver.jid);
                }
                else {
                    selected_driver = driver;
                }

            });
            if (selected_driver) {
                $scope.confirmDriver(selected_driver);
            }
        };
        console.log("before add handler");
        if (!XmppAuth.on_message_handler) {
            XmppAuth.on_message_handler = XmppAuth.connection.addHandler($scope.on_message, null, "message", "chat");
            console.log("after add handler");
        }



    });
