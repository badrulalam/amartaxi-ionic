/**
 * Created by rbn on 5/14/15.
 */
angular.module('starter')

    .controller('PassengerDestinationPointController', function( $ionicScrollDelegate, $ionicModal, $timeout, $scope,$http,$cordovaGeolocation,OurLocationPoint,$rootScope,$state) {
        $scope.searchLoc = "";
        $scope.locDropDown = "all";
        geocoder = new google.maps.Geocoder();
        $scope.googleNearByLocations = {};
        // start
        $scope.showhide = {};
        $scope.showhide.data = null;
        $scope.showhide.title = "Foursquare";
        $scope.showhide.favourite = false;
        $scope.showhide.places = false;
        $scope.showhide.google = false;
        $scope.showhide.foursquare = false;
        $scope.showhide.searchmap = true;
        $ionicScrollDelegate.resize();


        $scope.favClicked = function(){
            $scope.showhide.title = "Favourite";

            $scope.showhide.favourite = true;
            $scope.showhide.places = false;
            $scope.showhide.google = false;
            $scope.showhide.foursquare = false;
            $scope.showhide.searchmap = false;
            $ionicScrollDelegate.resize();
            $ionicScrollDelegate.scrollTop();
            $scope.openDestinationModal();
        };

        $scope.placesClicked = function(){
            $scope.showhide.title = "Our place";
            $scope.showhide.favourite = false;
            $scope.showhide.places = true;
            $scope.showhide.google = false;
            $scope.showhide.foursquare = false;
            $scope.showhide.searchmap = false;
            $ionicScrollDelegate.resize();
            $ionicScrollDelegate.scrollTop();
            $scope.openDestinationModal();
        };
        $scope.googleClicked = function(){
            $scope.showhide.title = "Google";
            $scope.showhide.favourite = false;
            $scope.showhide.places = false;
            $scope.showhide.google = true;
            $scope.showhide.foursquare = false;
            $scope.showhide.searchmap = false;
            $ionicScrollDelegate.resize();
            $ionicScrollDelegate.scrollTop();
            $scope.openDestinationModal();
        };
        $scope.fsClicked = function(){
            $scope.showhide.title = "Foursquare";
            $scope.showhide.favourite = false;
            $scope.showhide.places = false;
            $scope.showhide.google = false;
            $scope.showhide.foursquare = true;
            $scope.showhide.searchmap = false;
            $ionicScrollDelegate.resize();
            $ionicScrollDelegate.scrollTop();
            $scope.openDestinationModal();
        };
        $scope.smClicked = function(){
            $scope.showhide.title = "Google Auto-complete";
            $scope.showhide.favourite = false;
            $scope.showhide.places = false;
            $scope.showhide.google = false;
            $scope.showhide.foursquare = false;
            $scope.showhide.searchmap = true;
            $ionicScrollDelegate.resize();
            $ionicScrollDelegate.scrollTop();
            $scope.openDestinationModal();
        };
        $scope.place = {};
        $scope.place.geometry = {};
        $scope.place.geometry.location = {};
        $scope.maplocation = {};
        $scope.maplocation.latitude = "23.792496";
        $scope.maplocation.longitude = "90.407806";
        $scope.maplocation.locationname = "Gulshan 1";
        $scope.types = "['establishment']";
        $scope.placeChanged = function() {
            $scope.place = this.getPlace();
            console.log("in pace now", $scope.place);
            $scope.maplocation.latitude = $scope.place.geometry.location.lat();
            $scope.maplocation.longitude = $scope.place.geometry.location.lng();
            $scope.maplocation.locationname =  $scope.place.formatted_address;
            $rootScope.updateDestination($scope.maplocation.latitude,$scope.maplocation.longitude,$scope.maplocation.locationname);

        };


        $.post( "https://api.foursquare.com/v2/venues/search?ll=23.78670,90.41496&client_id=DRLQHEVBRZCRSSWLJRUO3AZ1PSZVTGXBA0P3U0Q55JZOU4R0&client_secret=U2HCI2IG1X5NZOT5SXGNV3PQDWPC02HTROE3P4EKVHSKCWV5&v=20150518", function( data ) {
            //console.log(data.response.venues);
            $scope.fourSquare = data.response.venues;
        });
        $scope.loadFSData = function(data){
            data  = JSON.parse(data);
            //console.log("https://api.foursquare.com/v2/venues/search?ll="+data.latitude+","+data.longitude+"&client_id=DRLQHEVBRZCRSSWLJRUO3AZ1PSZVTGXBA0P3U0Q55JZOU4R0&client_secret=U2HCI2IG1X5NZOT5SXGNV3PQDWPC02HTROE3P4EKVHSKCWV5&v=20150518");
            console.log(data);
            console.log(data.latitude+","+data.longitude);
            $rootScope.ionicLoadingShow();
            $.post("https://api.foursquare.com/v2/venues/search?ll="+data.latitude+","+data.longitude+"&client_id=DRLQHEVBRZCRSSWLJRUO3AZ1PSZVTGXBA0P3U0Q55JZOU4R0&client_secret=U2HCI2IG1X5NZOT5SXGNV3PQDWPC02HTROE3P4EKVHSKCWV5&v=20150518", function (data) {
                //console.log(data.response.venues);
                $scope.fourSquare = data.response.venues;
                $rootScope.ionicLoadingHide();
            });
        };
        $scope.loadGoogleData = function(data){
            data  = JSON.parse(data);
            //console.log("https://api.foursquare.com/v2/venues/search?ll="+data.latitude+","+data.longitude+"&client_id=DRLQHEVBRZCRSSWLJRUO3AZ1PSZVTGXBA0P3U0Q55JZOU4R0&client_secret=U2HCI2IG1X5NZOT5SXGNV3PQDWPC02HTROE3P4EKVHSKCWV5&v=20150518");
            console.log(data);
            console.log(data.latitude+","+data.longitude);
            $rootScope.ionicLoadingShow();
            var latlng = new google.maps.LatLng(data.latitude,data.longitude);

            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        $scope.googleNearByLocations = results;
                        //console.log($scope.googleNearByLocations);
                        //$scope.pickuppoint = results[1].formatted_address;
                        //map.panTo(latlng);

                    } else {
                        console.log('No results found');
                    }
                } else {
                    console.log('Geocoder failed due to: ',status);
                }
                $rootScope.ionicLoadingHide();
            });
        };
        $scope.getFoursqurePoint= function(lat,long){
            //$.post( "https://api.foursquare.com/v2/venues/search?ll="+lat+","+long+"&client_id=DRLQHEVBRZCRSSWLJRUO3AZ1PSZVTGXBA0P3U0Q55JZOU4R0&client_secret=U2HCI2IG1X5NZOT5SXGNV3PQDWPC02HTROE3P4EKVHSKCWV5&v=20150518", function( data ) {
            //    //console.log(data.response.venues);
            //    $scope.fourSquare = data.response.venues;
            //});
            console.log(lat, long);
        };
        //  our location start //
        $scope.ourlocation = {};

        $scope.getMyPosition = function () {
            var posOptions = {timeout: 10000, enableHighAccuracy: false};
            $cordovaGeolocation
                .getCurrentPosition(posOptions)
                .then(function (position) {
                    var lat = position.coords.latitude;
                    var long = position.coords.longitude;
                    $scope.latitude  = lat;
                    $scope.longitude = long;
                    //console.log("lat: " + lat + " long: " + long);
                    var latlng = new google.maps.LatLng($scope.latitude,$scope.longitude);

                    geocoder.geocode({'latLng': latlng}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[1]) {
                                $scope.googleNearByLocations = results;
                                //console.log($scope.googleNearByLocations);
                                //$scope.pickuppoint = results[1].formatted_address;
                                //map.panTo(latlng);

                            } else {
                                console.log('No results found');
                            }
                        } else {
                            console.log('Geocoder failed due to: ',status);
                        }
                    });
                }, function (err) {
                    console.log(err);
                });
        };
        $scope.getMyPosition();



        $ionicModal.fromTemplateUrl('templates/passenger/passenger_destination_point_modal.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.modal= modal;
        });

        // Triggered in the login modal to close it
        $scope.closeDestinationModal = function() {
            $scope.modal.hide();
        };

        // Open the login modal
        $scope.openDestinationModal = function() {
            $scope.modal.show();
        };


        $scope.doDestinationModal = function() {
            if($scope.showhide.favourite){
                if($scope.showhide.data){
                    console.log('Doing favourite', $scope.showhide.data);
                }else{
                    console.log('Doing favourite is null');
                }

            }
            if($scope.showhide.places){
                if($scope.showhide.data){
                    $scope.ourLocClicked($scope.showhide.data);
                    console.log('Doing places', $scope.showhide.data);
                }else{
                    console.log('Doing places is null');
                }

            }
            if($scope.showhide.google){
                if($scope.showhide.data){
                    $scope.googleLocClicked($scope.showhide.data);
                    console.log('Doing google', $scope.showhide.data);
                }else{
                    console.log('Doing google is null');
                }

            }
            if($scope.showhide.foursquare){
                if($scope.showhide.data){
                    $scope.fourSquareLocClicked($scope.showhide.data);
                    console.log('Doing foursquare', $scope.showhide.data);
                }else{
                    console.log('Doing foursquare is null');
                }

            }
            if($scope.showhide.searchmap){
                if($scope.showhide.data){
                    console.log('Doing searchmap', $scope.showhide.data);
                }else{
                    console.log('Doing searchmap is null');
                }

            }
            //console.log('Doing login', $scope.loginData);

            // Simulate a login delay. Remove this and replace with your login
            // code if using a login system
            //console.log('alert age');
            $timeout(function() {
                $scope.showhide.data = null;
                $scope.closeDestinationModal();
            }, 500);
            //console.log('alert pore');

        };

        $scope.googleLocClicked = function(loc) {
            //console.log('location',loc);
            $rootScope.updateDestination(loc.geometry.location.lat(),loc.geometry.location.lng(),loc.formatted_address);
           // $state.go('app.passenger_home');
        };

        $scope.fourSquareLocClicked = function(fsq){
            //console.log('foursquareclicked',fsq);
            $rootScope.updateDestination(fsq.location.lat,fsq.location.lng,fsq.name);
           // $state.go('app.passenger_home');
        };

        $scope.ourLocClicked = function(loc){
            //console.log('ourlocationcliked',loc);
            $rootScope.updateDestination(loc.latitude,loc.longitude,loc.nameBn);
          //  $state.go('app.passenger_home');
        };
        $scope.mapSelectButtonClicked = function(){
            //console.log('ourlocationcliked',loc);
            $rootScope.updateDestination($scope.maplocation.latitude,$scope.maplocation.longitude,$scope.maplocation.locationname);
            //$state.go('app.passenger_home');
        };

        $scope.searchValue= function(value, type)
        {
            OurLocationPoint.findByString(value, type).success(function(response){
                $scope.ourlocation = response;
                // console.log(response);
            });
        };
        //$scope.findDriver = function(){
        //    $scope.$parent.isActiveMapInteraction.pickup = false;
        //    $scope.$parent.isActiveMapInteraction.destination = false;
        //    $scope.hideMyMarker();
        //    $rootScope.params.directionStatus = 'find';
        //    $state.go('app.passenger_find_driver');
        //};
        $scope.findDriver = function () {
            console.log($rootScope.params.rideType);
            if ($rootScope.params.selectedPickUpPoint.latitude === '') {
                $rootScope.showAlert("Alert!", "Please Select Pickup Point");
            }
            else if ($rootScope.params.selectedDestinationPoint.latitude === '') {
                $rootScope.showAlert("Alert!", "Please Select Destination Point");
            }
            else if ($rootScope.params.rideType === 0) {
                $rootScope.showAlert("Alert!", "Please Select Ride Type");
            }
            else {
                $scope.$parent.isActiveMapInteraction.pickup = false;
                $scope.$parent.isActiveMapInteraction.destination = false;
                $scope.hideMyMarker();
                $rootScope.params.directionStatus = 'find';
                $state.go('app.passenger_find_driver');
            }
        };
        $scope.goToPickup = function(){
            $scope.$parent.isActiveMapInteraction.pickup = true;
            $scope.$parent.isActiveMapInteraction.destination = false;

            $scope.isActiveMapInteraction.showMarker = true;
            // $scope.isActiveMapInteraction.showMarker = true;

            $state.go('app.passenger_pickup_point');
        };
        $scope.searchValue( $scope.searchLoc ,$scope.locDropDown );
        OurLocationPoint.findByString("", 'police').success(function (response) {
            $scope.FourSquareZone = response;
            console.log(response);
        });
    });
