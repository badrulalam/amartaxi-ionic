/**
 * Created by rbn on 5/14/15.
 */
angular.module('starter')
    .controller('PassengerRegisterController', function($scope, $stateParams,Auth,$state,$cordovaOauth,$cordovaToast) {
        console.log("passenger Register");


        //
        //oauth google
        $scope.googleLogin = function() {
            $cordovaOauth.google("843765943613-j59f6iutqo9iehj2sb9remt84s8umsv7.apps.googleusercontent.com", ["email"]).then(function(result) {
                console.log("Response Object -> " + JSON.stringify(result));
            }, function(error) {
                console.log("Error -> " + error);
            });
        };

        //end oauth google
        //oauth google
        $scope.facebookLogin = function() {
            $cordovaOauth.facebook("435455849965190", ["email"]).then(function(result) {
                console.log("Response Object -> " + JSON.stringify(result));
            }, function(error) {
                console.log("Error -> " + error);
            });
        };

        //end oauth google
        //
        $scope.submitForm = function(isValid) {
            // check to make sure the form is completely valid
            if (isValid) {
                console.log("yes i am in");
                // alert('our form is amazing');
            }
        };

        $scope.passenger = {};
        $scope.passengerRegister = function(cred){
            //console.log(cred.email);
            console.log(cred);
            Auth.register(cred).success(function (result) {
                console.log("register succeded");
                $state.go('app.passenger_home');
            }).error(function (err) {
                if(err = 'mobile found on passenger'){
                    $cordovaToast
                        .show('Registration Failed, Mobile Number already exists', 'short', 'center')
                        .then(function(success) {
                            // success
                        }, function (error) {
                            // error
                        });
                }
                else
                {
                    $cordovaToast
                        .show('Registration Failed, Something went wrong', 'short', 'center')
                        .then(function(success) {
                            // success
                        }, function (error) {
                            // error
                        });
                }
                console.log("registration failed",err);
                // $scope.showToast('Login failed, Please Try Again', 'short','center');
            });
        }
    });
