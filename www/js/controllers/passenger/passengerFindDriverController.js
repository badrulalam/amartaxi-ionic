/**
 * Created by rbn on 5/14/15.
 */
angular.module('starter')

    .controller('PassengerFindDriverController', function($scope, $http, $state, $stateParams,Driver,$timeout,$rootScope,XmppAuth, CurrentUser, Booking) {
        $rootScope.ionicLoadingShow();
        console.log("passenger find Driver");

      if(!$rootScope.params)
      {
          $state.go('app.passenger_home')
      }



        console.log('pickup latitude',$rootScope.params.selectedPickUpPoint.latitude);
        $scope.from = $rootScope.params.selectedPickUpPoint.latitude+","+$rootScope.params.selectedPickUpPoint.longitude;
        $scope.to = $rootScope.params.selectedDestinationPoint.latitude+","+$rootScope.params.selectedDestinationPoint.longitude;
        $scope.fromName = $rootScope.params.selectedPickUpPoint.locname;
        $scope.toName = $rootScope.params.selectedDestinationPoint.locname;
        $scope.markers = [];
        $scope.note = {};
        $scope.driverNo = 0;



        console.log("from ",$scope.from );
        console.log("to ",$scope.to );
        console.log("fromName ",$scope.fromName );
        console.log("toName ",$scope.toName );
        console.log("markers ",$scope.markers );
        console.log("note ",$scope.note );
        console.log("driverNo ",$scope.driverNo );

        Driver.available().success(function(result){
            console.log("result info"+result);
            $rootScope.allDrivers = result;
            result.forEach(function(driver){
                console.log(driver.current_lat+","+driver.current_long);
                //$scope.neighborhoods.push(new google.maps.LatLng(driver.current_lat+", "+driver.current_long));
                console.log('inside driver foreach');
                $scope.markers.push([driver.current_lat, driver.current_long]);
                $rootScope.params.driverPositions.push({lat:driver.current_lat, lng: driver.current_long});
                $scope.driverNo += 1;
            });
            console.log($rootScope.params.driverPositions);
            $rootScope.ionicLoadingHide();
        });

        var x2js = new X2JS();
        $scope.sendMessage = function () {
          //  set booking by passenger
            var booking = {
                pickup_point_lat : $rootScope.params.selectedPickUpPoint.latitude,
                pickup_point_long : $rootScope.params.selectedPickUpPoint.longitude,
                pickup_point_name : $rootScope.params.selectedPickUpPoint.locname,
                destination_point_lat : $rootScope.params.selectedDestinationPoint.latitude,
                destination_point_long : $rootScope.params.selectedDestinationPoint.longitude,
                destination_point_name : $rootScope.params.selectedDestinationPoint.locname,

                ride_type: $rootScope.params.rideType,
                passenger_note: $scope.note.note,
                discount: $scope.note.discount,
                is_complete : false,
                is_cancelled : false,
                is_active: true,
                passenger : CurrentUser.user().id
                //rating: 3.0
            };



            Booking.setByPassenger(booking).success(function (response) {
                console.log('setByPassenger object is ', response);
                $rootScope.currentBookingObject = response;
                $scope.message =  {};
                $scope.message.from = $scope.from;
                $scope.message.to = $scope.to;
                $scope.message.note = $scope.note.note;
                $scope.message.discount = $scope.note.discount;
                $scope.message.user = CurrentUser.userInfo();
                var result = $rootScope.allDrivers;
                result.forEach(function(driver){
                    XmppAuth.send_message(driver.jid+"@amartaxi.com",JSON.stringify($rootScope.currentBookingObject));
                    console.log(driver.jid);
                });
                $scope.$parent.hideDriverMarkers();
                $state.go('app.passenger_contancting_driver');
            }).error(function(err){
                console.log('setByPassenger error is',err);
            });




        };

        $scope.on_message = function (message) {
            console.log('accept reject reply',message);
            return true;
        };


        $(document).bind('connected', function () {

            console.log("limon connected");

            var iq = $iq({type: 'get'}).c('query', {xmlns: 'jabber:iq:roster'});
            console.log(iq.toString());
            XmppAuth.connection.sendIQ(iq, XmppAuth.on_roster);
            XmppAuth.connection.addHandler(XmppAuth.on_roster_changed, "jabber:iq:roster", "iq", "set");
            XmppAuth.connection.addHandler($scope.on_message, null, "message", "chat");
        });

    });
