/**
 * Created by rbn on 5/14/15.
 */
angular.module('starter')
    .controller('PassengerCurrentBookingController', function($scope,$state,$stateParams, $http, Booking, $ionicPopup, $interval) {

        $scope.data = {};
        $scope.progressData = {
            max : 30,
            current : 7,
            interval: 1000};

        $scope.changeTime = function(){
          if($scope.progressData.current < $scope.progressData.max) {
              $scope.progressData.current++;
          } else {
              $scope.progressData.current = 0;
          }
            console.log()
        };
        //$scope.changeTimeInterval = $interval($scope.changeTime, $scope.progressData.interval);

        $scope.rateDriver = function(){


            //alert();
            var myPopup = $ionicPopup.show({
                template: '<rating ng-model="data.rating" max=5 readonly=false  on-leave="overStar = null"></rating>',
                title: 'Rate Driver',
                //subTitle: 'Please use normal things',
                scope: $scope,
                buttons: [
                    { text: 'Cancel'},
                    {
                        text: '<b>Save</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                            if (!$scope.data.rating) {
                                //don't allow the user to close unless he enters wifi password
                                e.preventDefault();
                            } else {
                                //console.log($scope.data.rating);
                                //return $scope.data.rating;


                                Booking.rateBooking({bookingId: $scope.current_booking.id, rating: $scope.data.rating})
                                    .success(function (response) {
                                        $scope.current_booking.rating = response.rating;
                                        $scope.data.rating = null;

                                    }).error(function(err){
                                        console.log("after rating response error", err);

                                    });
                            }
                        }
                    }
                ]
            });
        };
        $scope.cancelRide = function(){
          //alert();



            var confirmPopup = $ionicPopup.confirm({
                    title: 'Alert',
                    template: 'Are you sure you want to complete this ride?'
                });
                confirmPopup.then(function(res) {
                    if(res) {
                        console.log('You are sure');
                        Booking.cancelByPassenger({bookingId: $scope.current_booking.id})
                            .success(function (response) {

                                $scope.current_booking.is_active = response.is_active;
                                $scope.current_booking.is_cancelled = response.is_cancelled ;
                                $scope.current_booking.cancelBy = response.cancelBy ;
                                $scope.current_booking.is_complete = response.is_complete ;
                                $scope.current_booking.completeBy = response.completeBy ;
                            }).error(function(err){

                            });
                    } else {
                        console.log('You are not sure');
                    }
                });
        };
        $scope.completeRide = function(){
          //alert();



            var confirmPopup = $ionicPopup.confirm({
                    title: 'Alert',
                    template: 'Are you sure you want to complete this ride?'
                });
                confirmPopup.then(function(res) {
                    if(res) {
                        console.log('You are sure');
                        Booking.completeByPassenger({bookingId: $scope.current_booking.id})
                            .success(function (response) {
                                $scope.current_booking.is_active = response.is_active;
                                $scope.current_booking.is_cancelled = response.is_cancelled ;
                                $scope.current_booking.cancelBy = response.cancelBy ;
                                $scope.current_booking.is_complete = response.is_complete ;
                                $scope.current_booking.completeBy = response.completeBy ;

                            }).error(function(err){

                            });
                    } else {
                        console.log('You are not sure');
                    }
                });
        };


    });
