/**
 * Created by rbn on 5/14/15.
 */
angular.module('starter')

    .controller('PassengerTrackDriverController', function ($scope, $rootScope, $stateParams, XmppAuth, $rootScope, $interval, $cordovaSms, Booking, $state, $ionicPopup) {
        $rootScope.params.directionStatus = 'track';

        $scope.originCoord = {};
        $scope.originCoord.lat = 23.792496;
        $scope.originCoord.long = 90.407806;

        $scope.changeDriverPosition = function () {
            $rootScope.params.driverLocationPoint.latitude = $scope.originCoord.lat;
            $rootScope.params.driverLocationPoint.longitude = $scope.originCoord.long;
            console.log("Driver position - ", $rootScope.params.driverLocationPoint);
        };
        $rootScope.changeDriverInterval = $interval($scope.changeDriverPosition, 3000);
        $scope.on_message = function (message) { //console.log(message);  //*************************************************
            var full_jid = $(message).attr('from');
            var jid = Strophe.getBareJidFromJid(full_jid);
            var jid_id = XmppAuth.jid_to_id(jid);

            console.log("on_message", jid);


            var composing = $(message).find('composing');
            if (composing.length > 0) {
                $('#chat-' + jid_id + ' .chat-messages').append(
                    "<div class='chat-event'>" +
                    Strophe.getNodeFromJid(jid) +
                    " is typing...</div>");

                //Gab.scroll_chat(jid_id);
            }

            var body = $(message).find("html > body");

            if (body.length === 0) {
                body = $(message).find('body');
                if (body.length > 0) {
                    body = body.text()
                } else {
                    body = null;
                }
            } else {
                body = body.contents();

                var span = $("<span></span>");
                body.each(function () {
                    if (document.importNode) {
                        $(document.importNode(this, true)).appendTo(span);
                    } else {
                        // IE workaround
                        span.append(this.xml);
                    }
                });

                body = span;
            }

            if (body) {
                // remove notifications since user is now active
                $('#chat-' + jid_id + ' .chat-event').remove();

                // add the new message
                $('#chat-' + jid_id + ' .chat-messages').append(
                    "<div class='chat-message'>" +
                    "&lt;<span class='chat-name'>" +
                    Strophe.getNodeFromJid(jid) +
                    "</span>&gt;<span class='chat-text'>" +
                    "</span></div>");

                $('#chat-' + jid_id + ' .chat-message:last .chat-text')
                    .append(body);
                $rootScope.driverMessage = body;
                //$scope.changeDriverPosition(body);

                //console.log(body);
                var driverCoord = JSON.parse(body);
                console.log(driverCoord);
                $scope.originCoord.lat = driverCoord.latitude;
                $scope.originCoord.long = driverCoord.longitude;
                //console.log(driverCoord);
            }

            return true;
        };
        if ($rootScope.driverInfo) {
            console.log("before add handler");
            XmppAuth.connection.addHandler($scope.on_message, null, "message", "chat");
            console.log("after add handler");
        }


        ////////////////////////////sms////////////////////////////
        $scope.sendSMS = function () {
            //document.addEventListener("deviceready", function () {
            $cordovaSms
                .send('01670492520', 'Test SMS from amar taxi', options)
                .then(function () {
                    // Success! SMS was sent
                }, function (error) {
                    // An error occurred
                });
            //});
        };
        ////////////////////////sms end //////////////////////////


        //*********test function start****************
        $scope.sendMessage = function () {
            if ($rootScope.driverInfo) {
                console.log($rootScope.driverInfo);
                var jid = $rootScope.driverInfo.jid + "@amartaxi.com";
                XmppAuth.send_message(jid, "message from passenger")

            }
            else {
                console.log("rootscope driver info not found");
            }
        };
        //*********test function end  ****************


        //*********cancelBooking function start****************
        $scope.cancelBooking = function () {

            if ($rootScope.currentBookingObject) {

                console.log($rootScope.currentBookingObject);

                //duplicate code start
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Warning',
                    template: 'Are you want to cancel booking?'
                });
                confirmPopup.then(function (res) {
                    if (res) {
                        console.log('You are sure');

                        //console.log($rootScope.currentBookingObject);
                        // if($rootScope.currentBookingObject){
                        Booking.cancelByPassenger({bookingId: $rootScope.currentBookingObject.id})
                            .success(function (response) {
                                console.log(response);


                                if ($rootScope.driverInfo) {
                                    console.log($rootScope.driverInfo);
                                    var jid = $rootScope.driverInfo.jid + "@amartaxi.com";
                                    XmppAuth.send_message(jid, "cancelByPassenger");
                                }
                                else {
                                    console.log("rootscope driver info not found");
                                }


                                $interval.cancel($rootScope.changeDriverInterval);
                                $scope.$parent.mapReset();
                                $state.go('app.passenger_home');

                            }).error(function (err) {

                            });
                        //}
                    } else {
                        console.log('You are not sure');
                    }
                });
                //duplicate code end


                //*********cancelBooking function end  ****************


            }
            else {
                console("rootscope not found");
            }
        };
        $scope.driverReached = function () {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Warning',
                template: 'Are you sure driver reached?'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    console.log('You are sure');

                    if ($rootScope.driverInfo) {
                        console.log($rootScope.driverInfo);
                        var jid = $rootScope.driverInfo.jid + "@amartaxi.com";
                        XmppAuth.send_message(jid, "driverReachedByPassenger");
                    }
                    else {
                        console.log("rootscope driver info not found");
                    }
                    $interval.cancel($rootScope.changeDriverInterval);
                    $state.go('app.passenger_reached_destination');
                } else {
                    console.log('You are not sure');
                }
            });
        };

    });
