/**
 * Created by rbn on 5/14/15.
 */
angular.module('starter')

    .controller('PassengerRideTypeController', function ($http, Booking, $scope, $rootScope, $state, $stateParams, LocalService, $cordovaToast) {
        //console.log("in home controller");
        //if (!LocalService.get('auth_token')) {
        //    $state.go('passenger_login');
        //}
        //$scope.getMyParentPosition = function () {
        //    $scope.$parent.getMyPosition();
        //};
        //$scope.rideSelect = function (type) {
        //    $rootScope.params.rideType = type;
        //};
        //$scope.goNext = function () {
        //    console.log($rootScope.params.rideType);
        //    if ($rootScope.params.selectedPickUpPoint.latitude === '') {
        //        $rootScope.showAlert("Alert!", "Please Select Pickup Point");
        //    }
        //    else if ($rootScope.params.selectedDestinationPoint.latitude === '') {
        //        $rootScope.showAlert("Alert!", "Please Select Destination Point");
        //    }
        //    else if ($rootScope.params.rideType === 0) {
        //        $rootScope.showAlert("Alert!", "Please Select Ride Type");
        //    }
        //    else {
        //        $state.go('app.passenger_find_driver');
        //    }
        //};
        //$scope.catchYou = function () {
        //    $scope.$emit("catchYou");
        //}






        $scope.rideSelect = function (type) {
            $rootScope.params.rideType = type;
            try{
                if(type==="yellow"){
                    $cordovaToast
                        .show('yellow cab selected', 'short', 'center')
                        .then(function(success) {
                            // success
                        }, function (error) {
                            // error
                        });
                }
                else if(type==="black"){
                    $cordovaToast
                        .show('black cab selected', 'short', 'center')
                        .then(function(success) {
                            // success
                        }, function (error) {
                            // error
                        });
                }
                else if(type==="cng"){
                    $cordovaToast
                        .show('cng selected', 'short', 'center')
                        .then(function(success) {
                            // success
                        }, function (error) {
                            // error
                        });
                }
            }catch(e){
                console.log("Toast Errot",e);
            }


            //$state.go('app.passenger_home');


            Booking.getCarInfo($rootScope.params.rideType)
                .success(function(data){
                    $scope.carData = data;
                })
                .error(function(err){
                    $scope.carData = err;
                });
        };



    });
