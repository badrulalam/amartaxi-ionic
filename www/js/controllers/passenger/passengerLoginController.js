/**
 * Created by rbn on 5/14/15.
 */
angular.module('starter')
    .controller('PassengerLoginController', function ($scope, $state, $stateParams, Auth, $ionicPopup, $cordovaToast, $cordovaOauth, $http, $ionicLoading, $rootScope) {

        //google analytics
        //if(typeof analytics !== undefined) { analytics.trackView("PassengerLoginController"); }
        //
        //$scope.initEvent = function() {
        //    if(typeof analytics !== undefined) { analytics.trackEvent("Category", "Action", "Label", 25); }
        //};

        //end google analytics

        //oauth google
        $scope.googleLogin = function () {
            $cordovaOauth.google("843765943613-7tfh01k05e69v29nkgnqbt32c1gcbm5g.apps.googleusercontent.com", ["https://www.googleapis.com/auth/urlshortener", "https://www.googleapis.com/auth/userinfo.email"]).then(function (result) {
                console.log('OAuth: ');
                console.log(JSON.stringify(result));
            }, function (error) {
                console.log(error);
            });
        };
        //end oauth google
        $scope.passenger = {};

        $scope.passengerLogin = function (cred) {
         //alert(cred.email.match(/^([^@]*)@/)[1]);
            //console.log(cred.email);
            $rootScope.ionicLoadingShow();
            console.log(cred);
            Auth.login(cred).success(function (result) {
                console.log("login succeeded");
                $rootScope.ionicLoadingHide();
                $state.go('app.passenger_home');
            }).error(function (err) {
                $rootScope.ionicLoadingHide();
                console.log("login failed");
                // $scope.showToast('Login failed, Please Try Again', 'short','center');
                //if (err = 'invalid mobile number or password') {
                //    $cordovaToast
                //        .show('invalid mobile number or password', 'short', 'center')
                //        .then(function (success) {
                //            // success
                //        }, function (error) {
                //            // error
                //        });
                //}
                //else {
                //    $cordovaToast
                //        .show('Something went wrong. Please check Internet Connection', 'short', 'center')
                //        .then(function (success) {
                //            // success
                //        }, function (error) {
                //            // error
                //        });
                //}
                //$scope.showAlert(err.err)
//console.log(err);
                $rootScope.showAlert("Error", err.err);
            });
        };
        //$scope.showAlert = function (message) {
        //    var alertPopup = $ionicPopup.alert({
        //        title: 'Error!',
        //        template: message
        //    });
        //    alertPopup.then(function (res) {
        //        console.log('Thank you for not eating my delicious ice cream cone');
        //    });
        //};
        $scope.checkConnection= function() {
            var networkState = navigator.connection.type;

            var states = {};
            states[Connection.UNKNOWN]    = 'Unknown connection';
            states[Connection.ETHERNET]    = 'Ethernet connection';
            states[Connection.WIFI]       = 'WiFi connection';
            states[Connection.CELL_2G]    = 'Cell 2G connection';
            states[Connection.CELL_3G]    = 'Cell 3G connection';
            states[Connection.CELL_4G]    = 'Cell 4G connection';
            states[Connection.NONE]       = 'No network connection';

            alert('Connection type: ' + states[networkState]);
        }
    });
