/**
 * Created by rbn on 5/14/15.
 */
angular.module('starter')

    .controller('PassengerHomeController', function ($http, $scope, $rootScope, $state, $stateParams, LocalService) {
        console.log("in home controller");
        if (!LocalService.get('auth_token')) {
            $state.go('passenger_login');
        }
        $scope.getMyParentPosition = function () {
            $scope.$parent.getMyPosition();
        };

        $scope.goNext = function () {
            console.log($rootScope.params.rideType);
            if ($rootScope.params.selectedPickUpPoint.latitude === '') {
                $rootScope.showAlert("Alert!", "Please Select Pickup Point");
            }
            else if ($rootScope.params.selectedDestinationPoint.latitude === '') {
                $rootScope.showAlert("Alert!", "Please Select Destination Point");
            }
            else if ($rootScope.params.rideType === 0) {
                $rootScope.showAlert("Alert!", "Please Select Ride Type");
            }
            else {
                $scope.$parent.isActiveMapInteraction.pickup = false;
                $scope.$parent.isActiveMapInteraction.destination = false;
                $scope.hideMyMarker();
                $rootScope.params.directionStatus = 'find';
                $state.go('app.passenger_find_driver');
            }
        };
        $scope.catchYou = function () {
            $scope.$emit("catchYou");
        };
        $scope.goToPickupPoint = function(){
            $scope.$parent.isActiveMapInteraction.pickup = true;
            $scope.$parent.isActiveMapInteraction.destination = false;
            $scope.isActiveMapInteraction.showMarker = true;
            $state.go('app.passenger_pickup_point');
        };
        $scope.goToDestinationPoint = function(){
            $scope.$parent.isActiveMapInteraction.pickup = false;
            $scope.$parent.isActiveMapInteraction.destination = true;
            $state.go('app.passenger_destination_point');

        };

    });
