/**
 * Created by mahfuz on 9/2/15.
 */
angular.module('starter')
    .controller('DriverAppController', function($scope, $ionicModal, $timeout,Auth,$state,XmppAuth, $rootScope, $interval, $rootScope) {
        // Form data for the login modal
        $scope.loginData = {};


        $scope.logout = function(){
            if($rootScope.saveContPositoin){
                $interval.cancel($rootScope.saveContPositoin);
            }
            if($rootScope.sendContPositionToPassenger){
                $interval.cancel($rootScope.sendContPositionToPassenger);
            }
            if(XmppAuth.connection){
                XmppAuth.disconnect_me();
            }

            Auth.logout();
            $state.go('passenger_login');
        };
        $scope.goToHome = function(){
                $state.go('driver_app.driver_home');
        };
        $scope.goToRideHistory = function(){
                $state.go('driver_app.driver_riding_history');
        };


    });