/**
 * Created by rbn on 5/18/15.
 */
//https://api.foursquare.com/v2/venues/search?ll=23.78670,90.41496&oauth_token=KVOS5FW4XSOD1VHKS1ILGX2NSKL2Z0G1P2OS01X5GLXUZSUJ&v=20150518
//https://api.foursquare.com/v2/venues/search?ll=23.78670,90.41496&client_id=DRLQHEVBRZCRSSWLJRUO3AZ1PSZVTGXBA0P3U0Q55JZOU4R0&client_secret=U2HCI2IG1X5NZOT5SXGNV3PQDWPC02HTROE3P4EKVHSKCWV5&v=20150518

angular.module('starter.services')
    .factory('FourSquare', function ($http) {
        return {
            get: function () {
                var locations = $http.post('https://api.foursquare.com/v2/venues/search?ll=23.78670,90.41496&client_id=DRLQHEVBRZCRSSWLJRUO3AZ1PSZVTGXBA0P3U0Q55JZOU4R0&client_secret=U2HCI2IG1X5NZOT5SXGNV3PQDWPC02HTROE3P4EKVHSKCWV5&v=20150518').success(function (resp) {
                    console.log('Success', resp);
                }, function (err) {
                    console.error('ERR: ', err);
                });
                return locations;
            },
            all: function () {
                var blogs = $http.get(Constants.BaseURL+'/blog').success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                });
                return blogs;
            }
        }
    })
    .factory('LatlngToLocationName', function ($http) {
        return {
            get: function (lat,long) {
                geocoder = new google.maps.Geocoder();
                var latlng = new google.maps.LatLng(lat, long);
                var locationName = "";
                geocoder.geocode({'latLng': latlng}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            console.log(results[1].formatted_address);
                            locationName = results[1].formatted_address;
                            return locationName;
                        } else {
                            console.log('No results found');
                        }
                    } else {
                        console.log('Geocoder failed due to: ' + status);
                    }
                });
                return locationName;
            }
        }
    })
    .factory('OurLocationPoint', function ($http, Constants) {
        return {
            education: function () {
                var locations = $http.get(Constants.BaseURL + '/educationpoint').success(function (resp) {
                    console.log("education load success: ");
                    console.log(resp);
                }, function (err) {
                    console.log("education load error: ");
                    console.log(err);
                });
                return locations;
            },
            police: function () {
                var locations = $http.get(Constants.BaseURL + '/policestationpoint').success(function (resp) {
                    console.log('Success', resp);
                }, function (err) {
                    console.log(err);
                    // err.status will contain the status code
                });
                return locations;
            },
            hospital: function () {
                var locations = $http.get(Constants.BaseURL + '/hospitalpoint').success(function (resp) {
                    console.log('Success', resp);
                }, function (err) {
                    console.error('ERR: ', err);
                    // err.status will contain the status code
                });
                return locations;
            },
            important: function () {
                var locations = $http.get(Constants.BaseURL + '/importantpoint').success(function (resp) {
                    //console.log('Success', resp);
                }, function (err) {
                    console.error('ERR: ', err);
                    // err.status will contain the status code
                });
                return locations;
            },
            findByString: function (searchKey, type) {console.log(type);
var query ="";
                if(type=="education")
                {
                    query = Constants.BaseURL + '/educationpoint?where={"nameEn":{"contains":"'+searchKey+'"}}&limit=10';
                }
                else if(type=="police")
                {
                    query = Constants.BaseURL + '/policestationpoint?where={"nameEn":{"contains":"'+searchKey+'"}}&limit=10';
                }
                else if(type=="hospital")
                {
                    query = Constants.BaseURL + '/hospitalpoint?where={"nameEn":{"contains":"'+searchKey+'"}}&limit=10';
                }
                else if(type=="important")
                {
                    query = Constants.BaseURL + '/importantpoint?where={"nameEn":{"contains":"'+searchKey+'"}}&limit=10';
                }
                else
                {
                    query = Constants.BaseURL + '/importantpoint?where={"nameEn":{"contains":"'+searchKey+'"}}&limit=10';
                }




                var locations = $http.get(query).success(function (resp) {
                    //console.log('Success', resp);
                }, function (err) {
                    //console.error('ERR: ', err);
                    // err.status will contain the status code
                });
                return locations;
            },
        }
    });
