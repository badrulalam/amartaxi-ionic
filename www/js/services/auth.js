///**
// * Created by rbn on 5/13/15.
// */
//angular.module('starter.services')
//    .factory('PassengerAuth', function ($http, LocalService, AccessLevels, Constants) {
//        return {
//            authorize: function (access) {
//                if (access === AccessLevels.user) {
//                    return this.isAuthenticated();
//                } else {
//                    return true;
//                }
//            },
//            isAuthenticated: function () {
//                return LocalService.get('auth_token');
//            },
//            login: function (credentials) {
//                //104.236.249.183
//                console.log("inside login auth: " + JSON.stringify(credentials).toString());
//                var login = $http.post(Constants.BaseURL + '/auth/authenticate', credentials).
//                    success(function (result) {
//                        console.log("auth success");
//
//                        LocalService.set('auth_token', JSON.stringify(result));
//                    }).error(function (result) {
//                        console.log("login error: " + result);
//                    });
//                //console.log("webroot: " + $rootScope.webRoot);
//                return login;
//            },
//            logout: function () {
//                // The backend doesn't care about logouts, delete the token and you're good to go.
//                LocalService.unset('auth_token');
//            },
//            register: function (formData) {
//                LocalService.unset('auth_token');
//                var register = $http.post(Constants.BaseURL + '/auth/register', formData);
//                register.success(function (result) {
//                    LocalService.set('auth_token', JSON.stringify(result));
//                });
//                return register;
//            }
//        }
//    })
//    .factory('DriverAuth', function ($http, LocalService, AccessLevels, Constants, $rootScope) {
//        return {
//            authorize: function (access) {
//                if (access === AccessLevels.user) {
//                    return this.isAuthenticated();
//                } else {
//                    return true;
//                }
//            },
//            isAuthenticated: function () {
//                return LocalService.get('auth_token');
//            },
//            login: function (credentials) {
//                console.log(credentials);
//                //104.236.249.183
//                console.log("inside login auth: " + JSON.stringify(credentials).toString());
//                var login = $http.post(Constants.BaseURL + '/carauth/authenticate', credentials).
//                    success(function (result) {
//                        console.log("auth success");
//
//                        LocalService.set('auth_token', JSON.stringify(result));
//                        $rootScope.CurrentDriver = result;
//                    }).error(function (result) {
//                        console.log( result);
//                    });
//                //console.log("webroot: " + $rootScope.webRoot);
//                return login;
//            },
//            logout: function () {
//                // The backend doesn't care about logouts, delete the token and you're good to go.
//                LocalService.unset('auth_token');
//            },
//            register: function (formData) {
//                console.log(formData);
//                LocalService.unset('auth_token');
//                var register = $http.post(Constants.BaseURL + '/carauth/register', formData);
//                register.success(function (result) {
//                    LocalService.set('auth_token', JSON.stringify(result));
//                    $rootScope.CurrentDriver = result;
//                });
//                return register;
//            }
//        }
//    })
//    .factory('AuthInterceptor', function ($q, $injector) {
//        var LocalService = $injector.get('LocalService');
//
//        return {
//            request: function (config) {
//                var token;
//                if (LocalService.get('auth_token')) {
//                    token = angular.fromJson(LocalService.get('auth_token')).token;
//                }
//                if (token) {
//                    config.headers.Authorization = 'Bearer ' + token;
//                }
//                return config;
//            },
//            responseError: function (response) {
//                if (response.status === 401 || response.status === 403) {
//                    LocalService.unset('auth_token');
//                    $injector.get('$state').go('anon.login');
//                }
//                return $q.reject(response);
//            }
//        }
//    })
//    .config(function ($httpProvider) {
//        $httpProvider.interceptors.push('AuthInterceptor');
//    })
//    .factory('CurrentUser', function(LocalService) {
//        return {
//            user: function() {
//                console.log("in current user");
//                if (LocalService.get('auth_token')) {
//                    console.log("got auth token");
//                    return angular.fromJson(LocalService.get('auth_token')).user;
//                } else {
//                    return {};
//                }
//            },
//            car: function() {
//                console.log("in current user");
//                if (LocalService.get('auth_token')) {
//                    console.log("got auth token");
//                    return angular.fromJson(LocalService.get('auth_token')).car;
//                } else {
//                    return {};
//                }
//            }
//        };
//    });