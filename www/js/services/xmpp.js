/**
* Created by rbn on 5/13/15.
*/
angular.module('starter.services')
.service('XmppAuth', function(LocalService, $http,$state,  $rootScope) {
        var Gab = {
            connection: null,
            on_message_handler: null,
            start_time: null,
            log: function (msg) {
              console.log(msg);
            },
            jid_to_id: function (jid) {
                return Strophe.getBareJidFromJid(jid)
                    .replace(/@/g, "-")
                    .replace(/\./g, "-");
            },
            jid_to_node: function (jid) {
// console.log(Strophe.getNodeFromJid(jid));
// console.log(Strophe.getDomainFromJid(jid));
// console.log(Strophe.getResourceFromJid(jid));
// console.log(Strophe.getBareJidFromJid(jid));
              return Strophe.getNodeFromJid(jid);
            },
            on_roster: function (iq) {
                $(iq).find('item').each(function () {
                    var jid = $(this).attr('jid');
                    var name = $(this).attr('name') || jid;

                    // transform jid into an id
                    var jid_id = Gab.jid_to_id(jid);

                    console.log("on_roster",jid);

                    var contact = $("<li id='" + jid_id + "'>" +
                    "<div class='roster-contact offline'>" +
                    "<div class='roster-name'>" +
                    name +
                    "</div><div class='roster-jid'>" +
                    jid +
                    "</div></div></li>");

                    Gab.insert_contact(contact);
                });

                // set up presence handler and send initial presence
                Gab.connection.addHandler(Gab.on_presence, null, "presence");
                Gab.connection.send($pres());
            },

            pending_subscriber: null,

            on_presence: function (presence) { console.log(presence);
                var ptype = $(presence).attr('type');
                var from = $(presence).attr('from');
                var jid_id = Gab.jid_to_id(from);

                  console.log("on_presence",from);

                if (ptype === 'subscribe') {
                    // populate pending_subscriber, the approve-jid span, and
                    // open the dialog
                    Gab.pending_subscriber = from;
                    $('#approve-jid').text(Strophe.getBareJidFromJid(from));
                    $('#approve_dialog').dialog('open');
                } else if (ptype !== 'error') {
                    var contact = $('#roster-area li#' + jid_id + ' .roster-contact')
                        .removeClass("online")
                        .removeClass("away")
                        .removeClass("offline");
                    if (ptype === 'unavailable') {
                        contact.addClass("offline");
                    } else {
                        var show = $(presence).find("show").text();
                        if (show === "" || show === "chat") {
                            contact.addClass("online");
                        } else {
                            contact.addClass("away");
                        }
                    }

                    var li = contact.parent();
                    li.remove();
                    Gab.insert_contact(li);
                }

                // reset addressing for user since their presence changed
                var jid_id = Gab.jid_to_id(from);
                $('#chat-' + jid_id).data('jid', Strophe.getBareJidFromJid(from));

                return true;
            },

            on_roster_changed: function (iq) {
                $(iq).find('item').each(function () {
                    var sub = $(this).attr('subscription');
                    var jid = $(this).attr('jid');
                    var name = $(this).attr('name') || jid;
                    var jid_id = Gab.jid_to_id(jid);

                    console.log("on_roster_changed",jid);

                    if (sub === 'remove') {
                        // contact is being removed
                        $('#' + jid_id).remove();
                    } else {
                        // contact is being added or modified
                        var contact_html = "<li id='" + jid_id + "'>" +
                            "<div class='" +
                            ($('#' + jid_id).attr('class') || "roster-contact offline") +
                            "'>" +
                            "<div class='roster-name'>" +
                            name +
                            "</div><div class='roster-jid'>" +
                            jid +
                            "</div></div></li>";

                        if ($('#' + jid_id).length > 0) {
                            $('#' + jid_id).replaceWith(contact_html);
                        } else {
                            Gab.insert_contact($(contact_html));
                        }
                    }
                });

                return true;
            },

            on_message: function (message) { console.log(message);  //*************************************************
                var full_jid = $(message).attr('from');
                var jid = Strophe.getBareJidFromJid(full_jid);
                var jid_id = Gab.jid_to_id(jid);

                console.log("on_message",jid);

                // if ($('#chat-' + jid_id).length === 0) {
                //     $('#chat-area').tabs('add', '#chat-' + jid_id, jid);
                //     $('#chat-' + jid_id).append(
                //         "<div class='chat-messages'></div>" +
                //         "<input type='text' class='chat-input'>");
                // }
                //
                // $('#chat-' + jid_id).data('jid', full_jid);
                //
                // $('#chat-area').tabs('select', '#chat-' + jid_id);
                // $('#chat-' + jid_id + ' input').focus();

                var composing = $(message).find('composing');
                if (composing.length > 0) {
                    // $('#chat-' + jid_id + ' .chat-messages').append(
                    //     "<div class='chat-event'>" +
                    //     Strophe.getNodeFromJid(jid) +
                    //     " is typing...</div>");

                    //Gab.scroll_chat(jid_id);
                }

                var body = $(message).find("html > body");

                if (body.length === 0) {
                    body = $(message).find('body');
                    if (body.length > 0) {
                        body = body.text()
                    } else {
                        body = null;
                    }
                } else {
                    body = body.contents();

                    var span = $("<span></span>");
                    body.each(function () {
                        if (document.importNode) {
                            $(document.importNode(this, true)).appendTo(span);
                        } else {
                            // IE workaround
                            span.append(this.xml);
                        }
                    });

                    body = span;
                }

                if (body) {
                    // remove notifications since user is now active
                    $('#chat-' + jid_id + ' .chat-event').remove();

                    // add the new message
                    $('#chat-' + jid_id + ' .chat-messages').append(
                        "<div class='chat-message'>" +
                        "&lt;<span class='chat-name'>" +
                        Strophe.getNodeFromJid(jid) +
                        "</span>&gt;<span class='chat-text'>" +
                        "</span></div>");

                    $('#chat-' + jid_id + ' .chat-message:last .chat-text')
                        .append(body);
                        $rootScope.driverMessage = body;
                      //  window.passengerScope.changeDriverPosition();
                    //Gab.scroll_chat(jid_id);
                }

                return true;
            },

            scroll_chat: function (jid_id) {
                var div = $('#chat-' + jid_id + ' .chat-messages').get(0);
                div.scrollTop = div.scrollHeight;
            },

            presence_value: function (elem) {
                if (elem.hasClass('online')) {
                    return 2;
                } else if (elem.hasClass('away')) {
                    return 1;
                }

                return 0;
            },

            insert_contact: function (elem) { //console.log(elem);
                var jid = elem.find('.roster-jid').text();
                var pres = Gab.presence_value(elem.find('.roster-contact'));

                var contacts = $('#roster-area li');

                console.log("insert_contact",jid);

                if (contacts.length > 0) {
                    var inserted = false;
                    contacts.each(function () {
                        var cmp_pres = Gab.presence_value(
                            $(this).find('.roster-contact'));
                        var cmp_jid = $(this).find('.roster-jid').text();

                        if (pres > cmp_pres) {
                            $(this).before(elem);
                            inserted = true;
                            return false;
                        } else if (pres === cmp_pres) {
                            if (jid < cmp_jid) {
                                $(this).before(elem);
                                inserted = true;
                                return false;
                            }
                        }
                    });

                    if (!inserted) {
                        $('#roster-area ul').append(elem);
                    }
                } else {
                    $('#roster-area ul').append(elem);
                }
            },

            is_already_loggedin : function (){
                var token;
                if (LocalService.get('auth_chat_token')) {
                    token = angular.fromJson(LocalService.get('auth_chat_token'));
                    //console.log("yeah, he is already loggedin");
                    //console.log(token);
                    return token;
                }
                return false;
            },

            connect_me_now : function(userJid,userPass){  console.log(Strophe.Status);
                var conn = new Strophe.Connection(
                    'http://amartaxi.com:5280/http-bind');
                     // 'http://128.199.125.77:5505/http-bind');

                //'http://bosh.metajack.im:5280/xmpp-httpbind');

                conn.connect(userJid, userPass, function (status) {
                    console.log(status +" "+  userJid +" "+ userPass);
                    $rootScope.xmppStatus = status;
                    if(status==1){
                        $rootScope.xmppStyle={'color':'yellow'};
                    }else if(status==5){
                        //$state.go('app.passenger_home');
                        $rootScope.xmppStyle={'color':'green'};
                    }else if(status==6){
                        $rootScope.xmppStyle={'color':'red'};
                    }
                    //$rootScope.$apply();

                    if (status === Strophe.Status.CONNECTED) {
                        $(document).trigger('connected');
                        LocalService.set('auth_chat_token', JSON.stringify(
                            {
                                'authcid':Gab.connection.authcid,
                                'authzid':Gab.connection.authzid,
                                'authenticated':Gab.connection.authenticated,
                                'jid':Gab.connection.jid,
                                'pass':Gab.connection.pass
                            }
                        ));

                        $('.chat-login-form').hide();
                        $('.chat-logout-form').show();
                    } else if (status === Strophe.Status.DISCONNECTED) {
                        $(document).trigger('disconnected');
                        console.log(status);
                    }
                });

                Gab.connection = conn;
                //console.log(Gab.connection);
                // if(Gab.connection){
                //     //getting necessary info from current session and set at local storage
                //     LocalService.set('auth_chat_token', JSON.stringify(
                //         {
                //             'authcid':Gab.connection.authcid,
                //             'authzid':Gab.connection.authzid,
                //             'authenticated':Gab.connection.authenticated,
                //             'jid':Gab.connection.jid,
                //             'pass':Gab.connection.pass
                //         }
                //     ));
                //
                //     $('.chat-login-form').hide();
                //     $('.chat-logout-form').show();
                // }
            },

            disconnect_me : function (){
                Gab.connection.disconnect();
                Gab.connection = null;
                LocalService.unset('auth_chat_token');

            },
            online : function () {
              //var elementShow = Strophe.xmlElement('show', {}, 'chat');
              //var elementStatus = Strophe.xmlElement('status', {}, 'chat');
              var presence = $pres({from: Gab.connection.jid, xmlns: 'jabber:client', 'xmlns:stream': 'http://etherx.jabber.org/streams', version: '1.0'});
              //  .cnode(elementShow).up()
              //  .cnode(elementStatus);
                Gab.connection.send(presence.tree());
            },
            offline : function () { //console.log(jid);
              Gab.connection.send($pres({from: Gab.connection.jid, type: 'unavailable'}).tree());
            },
            away : function () {
              var elementShow = Strophe.xmlElement('show', {}, 'away');
              var elementStatus = Strophe.xmlElement('status', {}, 'away');
              var presense = $pres({from: Gab.connection.jid})
                .cnode(elementShow).up()
                .cnode(elementStatus);
                Gab.connection.send(presense.tree());
            },
            xa : function () {
              var elementShow = Strophe.xmlElement('show', {}, 'xa');
              var elementStatus = Strophe.xmlElement('status', {}, 'xa');
              var presense = $pres({from: Gab.connection.jid})
                .cnode(elementShow).up()
                .cnode(elementStatus);
                Gab.connection.send(presense.tree());
            },
            busy : function () {
              var elementShow = Strophe.xmlElement('show', {}, 'dnd');
              var elementStatus = Strophe.xmlElement('status', {}, 'do not disturb');
              var presense = $pres({from: Gab.connection.jid})
                .cnode(elementShow).up()
                .cnode(elementStatus);
                Gab.connection.send(presense.tree());
            },
            send_ping: function (to) {
              var ping = $iq({
              to: to,
              type: "get",
              id: "ping1"}).c("ping", {xmlns: "urn:xmpp:ping"});

              Gab.log("Sending ping to " + to + ".");
              Gab.start_time = (new Date()).getTime();
              Gab.connection.send(ping);
            },
              handle_pong: function (iq) {
                var elapsed = (new Date()).getTime() - Gab.start_time;
                Gab.log("Received pong from server in " + elapsed + "ms");
                Gab.connection.disconnect();
                return false;
              },
              send_message: function(jid, body){
                var message = $msg({to: jid,
                  "type": "chat"})
                .c('body').t(body).up()
                .c('active', {xmlns: "http://jabber.org/protocol/chatstates"});
                Gab.connection.send(message);
              },


              chenge_presense: function(){
                // var message = $msg({to: jid,
                //   "type": "chat"})
                // .c('body').t(body).up()
                // .c('active', {xmlns: "http://jabber.org/protocol/chatstates"});
                var press =  $iq({type: "get", id: "version1", to: "jabber.org"})
                    .c("query", {xmlns: "jabber:iq:version"});
               //var press = $pres({to: "limon@amartaxi.com"});
                 var sss = Gab.connection.send(press);
                console.log(sss);
              },
              register_me : function(jusername,jpassword){ console.log(jusername + " " + jpassword);
        var connection = new Strophe.Connection(
//          "http://bosh.metajack.im:5280/xmpp-httpbind");
//          "http://localhost/http-bind");
          'http://amartaxi.com:5280/http-bind');
        var registrationCallback = function (status) {
          if (status === Strophe.Status.REGISTER) {
            // fill out the fields
            connection.register.fields.username = jusername;
            connection.register.fields.password = jpassword;
            // calling submit will continue the registration process
            connection.register.submit();
          } else if (status === Strophe.Status.REGISTERED) {
            console.log("registered!");
            // calling login will authenticate the registered JID.
            connection.authenticate();
          } else if (status === Strophe.Status.CONFLICT) {
            console.log("Contact already existed!");
          } else if (status === Strophe.Status.NOTACCEPTABLE) {
            console.log("Registration form not properly filled out.")
          } else if (status === Strophe.Status.REGIFAIL) {
            console.log("The Server does not support In-Band Registration")
          } else if (status === Strophe.Status.CONNECTED) {
            // do something after successful authentication
          } else {
            // Do other stuff
          }
        };

        connection.register.connect("amartaxi.com", registrationCallback, 60, 1);


//        var registrationCallback = function (status) {
//
//          alert(status);
//          if (status === Strophe.Status.REGISTER) {
//            connection.register.fields.username = jusername;
//            connection.register.fields.name = jusername;
//            connection.register.fields.password = jpassword;
//            connection.register.submit();
//          } else if (status === Strophe.Status.REGISTERED) {
//            console.log("registered!");
//            connection.authenticate();
//          } else if (status === Strophe.Status.CONNECTED) {
//            $(document).trigger('connected');
//          } else if (status === Strophe.Status.DISCONNECTED) {
//            console.log("Disconnected from XMPP-Server");
//          }
//        };
//
////    connection.connect(data.jid, data.password, callback);
//        connection.register.connect("sunnahnikaah.com", registrationCallback, 60, 1);

        Gab.connection = connection;
        console.log("after registration and logged in");
        console.log(Gab.connection);
      },


        };
        //initially checking and if user refresh the page and the user already loggedin then it will automatically login with xmpp server
        var currentLoggedInUser = Gab.is_already_loggedin();
        console.log("currentLoggedInUser :\"\"\"", currentLoggedInUser );
        if(currentLoggedInUser){
            Gab.connect_me_now(currentLoggedInUser.jid,currentLoggedInUser.pass);
        }

        $(document).bind('connect', function (ev, data) {
         Gab.connect_me_now(data.jid,data.password);
        });



        $(document).bind('disconnected', function () {
         // console.log("limon disconnected");

            //console.log("gab disconnected");
            //alert("gab disconnected");
            Gab.connection = null;
            Gab.pending_subscriber = null;

            $('#roster-area ul').empty();
            $('#chat-area ul').empty();
            $('#chat-area div').remove();

            $('#login_dialog').dialog('open');
        });

        $(document).bind('contact_added', function (ev, data) {
            console.log("contact_added");
            var iq = $iq({type: "set"}).c("query", {xmlns: "jabber:iq:roster"})
                .c("item", data);
            Gab.connection.sendIQ(iq);

            var subscribe = $pres({to: data.jid, "type": "subscribe"});
            Gab.connection.send(subscribe);
        });

        return Gab;

    });
