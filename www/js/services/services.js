angular.module('starter.services', [])
    .constant('AccessLevels', {
        anon: 0,
        user: 1
    })
    .constant('Constants', {
      //BaseURL: 'http://localhost:1337' //localhost

      BaseURL: 'http://amartaxi.com:1337'  //server
    })
    .factory('LocalService', function () {
        return {
            get: function (key) {
                return localStorage.getItem(key);
            },
            set: function (key, val) {
                return localStorage.setItem(key, val);
            },
            unset: function (key) {
                return localStorage.removeItem(key);
            }
        }
    })
    .factory('Auth', function ($http, LocalService, AccessLevels, Constants, XmppAuth,CurrentUser, $rootScope) {
        return {
            authorize: function (access) {
                if (access === AccessLevels.user) {
                    return this.isAuthenticated();
                } else {
                    return true;
                }
            },
            isAuthenticated: function () {
                return LocalService.get('auth_token');
            },
            login: function (credentials) {
                //104.236.249.183
                console.log("inside login auth: " + JSON.stringify(credentials).toString());
                var login = $http.post(Constants.BaseURL + '/auth/authenticate', credentials).
                    success(function (result) {
                        console.log("auth success");
                        $rootScope.userType = "passenger"; console.log("$rootScope.userType ", $rootScope.userType );
                        LocalService.set('auth_token', JSON.stringify(result));
                        XmppAuth.connect_me_now(CurrentUser.user().jid + "@amartaxi.com", CurrentUser.user().jpassword);

                    }).error(function (result) {
                        console.log("login error: " , result);
                    });
                return login;
            },
            logout: function () {
                // The backend doesn't care about logouts, delete the token and you're good to go.
                LocalService.unset('auth_token');
            },
            register: function (formData) {
                LocalService.unset('auth_token');
                  var jid = formData.email.match(/^([^@]*)@/)[1];
                  var jpass = "password";
                  formData.jid = jid;
                  formData.jpassword = jpass;
                console.log(formData);
                var register = $http.post(Constants.BaseURL + '/auth/register', formData).success(function (result) {

                  console.log(result);
                    $rootScope.userType = "passenger"; console.log("$rootScope.userType ", $rootScope.userType )

                  LocalService.set('auth_token', JSON.stringify(result));
                    XmppAuth.connect_me_now(CurrentUser.user().jid + "@amartaxi.com", CurrentUser.user().jpassword);

                }).error(function (result) {
                        console.log("passenger req error: " , result);
                    });
                console.log(register);
                return register;
            },
            update: function (passengerInfo) {
                var status = $http.post(Constants.BaseURL + '/auth/update', passengerInfo).
                    success(function (result) {
                        console.log("successfully updated",result);

                    }).error(function (result) {
                        console.log("update error: " + result);
                    });
                return status;
            },
            updatePass: function (passInfo) {
                var status = $http.post(Constants.BaseURL + '/auth/updatepassword', passInfo).
                    success(function (result) {
                        console.log("successfully updated",result);

                    }).error(function (result) {
                        console.log("update error: " + result);
                    });
                return status;
            }
        }
    })
    .factory('DriverAuth', function ($http, LocalService, AccessLevels, Constants, $rootScope,XmppAuth,CurrentUser, $rootScope) {
        return {
            authorize: function (access) {
                if (access === AccessLevels.user) {
                    return this.isAuthenticated();
                } else {
                    return true;
                }
            },
            isAuthenticated: function () {
                return LocalService.get('auth_token');
            },
            login: function (credentials) {
                console.log(credentials);
                //104.236.249.183
                console.log("inside login auth: " + JSON.stringify(credentials).toString());
                var login = $http.post(Constants.BaseURL + '/driverauth/authenticate', credentials).
                    success(function (result) {
                      console.log(result);console.log("auth success");
                        $rootScope.userType = "driver"; console.log("$rootScope.userType ", $rootScope.userType )

                        LocalService.set('auth_token', JSON.stringify(result));
                        XmppAuth.connect_me_now(CurrentUser.car().jid + "@amartaxi.com", CurrentUser.car().jpassword);

                        $rootScope.CurrentDriver = result;
                    }).error(function (result) {
                        console.log( result);
                    });
                //console.log("webroot: " + $rootScope.webRoot);
                return login;
            },
            logout: function () {
                // The backend doesn't care about logouts, delete the token and you're good to go.
                LocalService.unset('auth_token');
            },
            register: function (formData) {
              var jid = formData.email.match(/^([^@]*)@/)[1];
              var jpass = "password";
              formData.jid = jid;
              formData.jpassword = jpass;
                console.log(formData);
                LocalService.unset('auth_token');
                var register = $http.post(Constants.BaseURL + '/driverauth/register', formData);
                register.success(function (result) {
                  console.log(result);
                  // XmppAuth.register_me(result.driver.jid, result.driver.jpassword);
                    $rootScope.userType = "driver"; console.log("$rootScope.userType ", $rootScope.userType )

                    LocalService.set('auth_token', JSON.stringify(result));
                    //XmppAuth.register_me(CurrentUser.car().jid + "@amartaxi.com", CurrentUser.car().jpassword);
                    console.log(result);
                    console.log(CurrentUser.car());

                    XmppAuth.connect_me_now(CurrentUser.car().jid + "@amartaxi.com", CurrentUser.car().jpassword);

                    $rootScope.CurrentDriver = result;
                }).error(function (result) {
                    console.log("driver registratoin error: " , result);
                });
                return register;
            }
        }
    })
    .factory('AuthInterceptor', function ($q, $injector) {
        var LocalService = $injector.get('LocalService');

        return {
            request: function (config) {
                var token;
                if (LocalService.get('auth_token')) {
                    token = angular.fromJson(LocalService.get('auth_token')).token;
                }
                if (token) {
                    config.headers.Authorization = 'Bearer ' + token;
                    //config.headers.Origin= 'http://evil.com';

                }
                return config;
            },
            responseError: function (response) {
                if (response.status === 401 || response.status === 403) {
                    LocalService.unset('auth_token');
                    //$injector.get('$state').go('passenger_login');
                }
                return $q.reject(response);
            }
        }
    })
    .config(function ($httpProvider) {
        $httpProvider.interceptors.push('AuthInterceptor');
    })
    .factory('CurrentUser', function(LocalService) {
        return {
            user: function() {
                console.log("in current user");
                if (LocalService.get('auth_token')) {
                    console.log("got auth token");
                    return angular.fromJson(LocalService.get('auth_token')).user;
                } else {
                    return {};
                }
            },
            userInfo: function() {
                console.log("in current user");
                if (LocalService.get('auth_token')) {
                    console.log("got auth token");
                    var data =  angular.fromJson(LocalService.get('auth_token')).user;
                    var user_info = {};

                     user_info.email = data.email;
                     user_info.name = data.name;
                     user_info.mobile = data.mobile;
                     user_info.id = data.id;
                     user_info.email = data.email;
                    return user_info;
                } else {
                    return {};
                }
            },

            car: function() {
                console.log("in current user");
                if (LocalService.get('auth_token')) {
                    console.log("got auth token");
                    return angular.fromJson(LocalService.get('auth_token')).driver;
                } else {
                    return {};
                }
            }
        };
    })
    .factory('CarPosition', function ($http, Constants) {

        return {
            all: function () {
                return chats;
            },
            remove: function (chat) {
                chats.splice(chats.indexOf(chat), 1);
            },
            get: function (chatId) {
                for (var i = 0; i < chats.length; i++) {
                    if (chats[i].id === parseInt(chatId)) {
                        return chats[i];
                    }
                }
                return null;
            },
            save: function (position,carid) {
                var postion = $http.post(Constants.BaseURL + '/carposition/create?carId='+carid, position)
                    .success(function (result) {
                        postion = result;
                        // console.log('success: '+JSON.stringify(result));
                    });
                return postion;
            }
        };
    })
    .factory('Driver', function ($http, Constants,CurrentUser) {

        return {

            saveCurrentPosition: function (position) {
              var driverId = CurrentUser.car().id;

                var data = {};
                data.lat = position.latitude;
                data.long = position.longitude;
                //  data.status = status;
                data.id = driverId;
                console.log(data);

                var postion = $http.post(Constants.BaseURL + '/driver/saveCurrentPosition', data)
                    .success(function (result) { console.log("save current posotion ",result);
                        postion = result;
                        // console.log('success: '+JSON.stringify(result));
                    });
                return postion;
            },
            all: function () {
                var drivers = $http.get(Constants.BaseURL+'/driver').success(function (resp) {
                    console.log('Success', resp);
                }, function (err) {
                    console.error('ERR', err);
                });
                return drivers;
            },
            available: function () {
                var drivers = $http.get(Constants.BaseURL+'/driver?status=available').success(function (resp) {
                    console.log('Success', resp);
                }, function (err) {
                    console.error('ERR', err);
                });
                return drivers;
            },
            changePresence: function (status) {
              var data = {};
              data.status = status;
              console.log(CurrentUser.car());
                var drivers = $http.put(Constants.BaseURL+'/driver/update/'+CurrentUser.car().id,data).success(function (resp) {
                    console.log('Success', resp);
                }, function (err) {
                    console.error('ERR', err);
                });
                return drivers;
            },
            savePosition: function (position,carid) {
                var postion = $http.post(Constants.BaseURL + '/driverposition/create?driverId='+carid, position)
                    .success(function (result) {
                        postion = result;
                        // console.log('success: '+JSON.stringify(result));
                    });
                return postion;
            }
        };
    })
    // .factory('CurrentDriver', function(LocalService) {
    //     return {
    //         car: function() {
    //             if (LocalService.get('auth_token')) {
    //                 return angular.fromJson(LocalService.get('auth_token')).user;
    //             } else {
    //                 return {};
    //             }
    //         }
    //     };
    // })
    .factory('Chats', function () {

        return {
            all: function () {
                return chats;
            },
            remove: function (chat) {
                chats.splice(chats.indexOf(chat), 1);
            },
            get: function (chatId) {
                for (var i = 0; i < chats.length; i++) {
                    if (chats[i].id === parseInt(chatId)) {
                        return chats[i];
                    }
                }
                return null;
            }
        };
    })
    .factory('Blog', function (Constants, $http) {
        return {
            all: function () {
                var blogs = $http.get(Constants.BaseURL+'/blog').success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                });
                return blogs;
            },
            //remove: function (chat) {
            //    chats.splice(chats.indexOf(chat), 1);
            //},
            get: function (blogId) {
                var blog = $http.get(Constants.BaseURL+'/blog?id='+blogId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                });
                return blog;
            }
        }
    })
    .factory('Category', function (Constants, $http) {
        return {
            all: function () {
                var categories = $http.get(Constants.BaseURL+'/category').success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                });
                return categories;
            },
            //remove: function (chat) {
            //    chats.splice(chats.indexOf(chat), 1);
            //},
            get: function (categoryId) {
                var category = $http.get(Constants.BaseURL+'/category?id='+categoryId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                });
                return category;
            }
        }
    })
    .factory('Thread', function (Constants, $http) {
        return {
            all: function () {
                var threads = $http.get(Constants.BaseURL+'/thread').success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                });
                return threads;
            },
            //remove: function (chat) {
            //    chats.splice(chats.indexOf(chat), 1);
            //},
            get: function (threadId) {
                var thread = $http.get(Constants.BaseURL+'/thread?name='+threadId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                });
                return thread;
            },
            getByName: function (threadName) {
                var thread = $http.get(Constants.BaseURL+'/thread?name='+threadName).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                });
                return thread;
            }
        }
    })
    .factory('Video', function (Constants, $http) {
        return {
            all: function () {
                var videos = $http.get(Constants.BaseURL+'/video').success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                });
                return videos;
            },
            //remove: function (chat) {
            //    chats.splice(chats.indexOf(chat), 1);
            //},
            get: function (videoId) {
                var video = $http.get(Constants.BaseURL+'/video?id='+videoId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                });
                return video;
            }
        }
    })
    .factory('User', function (Constants, $http) {
        return {
            all: function () {
                var users = $http.get(Constants.BaseURL+'/user').success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                });
                return users;
            },
            //remove: function (chat) {
            //    chats.splice(chats.indexOf(chat), 1);
            //},
            get: function (userId) {
                var user = $http.get(Constants.BaseURL+'/user?id='+userId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                });
                return user;
            },
            getByName: function (userName) {
                var user = $http.get(Constants.BaseURL+'/user?id='+userName).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                });
                return user;
            }
        }
    })
    .factory('Profile', function (Constants, $http) {
        return {
            // all: function () {
            //     var updateUser = $http.get(Constants.BaseURL+'/updateProfile').success(function (resp) {
            //         console.log('Success', resp);
            //         // For JSON responses, resp.data contains the result
            //         //$scope.restaurants = resp.data;
            //     }, function (err) {
            //         console.error('ERR', err);
            //         // err.status will contain the status code
            //     })
            //     return updateUser;
            // },
            // //remove: function (chat) {
            // //    chats.splice(chats.indexOf(chat), 1);
            // //},
            update: function (profileId) {
                var profile = $http.get(Constants.BaseURL+'/user?id='+profileId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                });
                return profile;
            }
        }
    })
    .factory('Friend', function (Constants, $http) {
        return {
            all: function () {
                var friends = $http.get(Constants.BaseURL+'/friend').success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                });
                return friends;
            },
            get: function (friendsId) {
                var friend = $http.get(Constants.BaseURL+'/friend?id='+friendsId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                });
                return friend;
            }
        }
    })


    .factory('Image', function (Constants, $http) {
        return {
            // all: function () {
            //     var friends = $http.get(Constants.BaseURL+'/friends').success(function (resp) {
            //         console.log('Success', resp);
            //         // For JSON responses, resp.data contains the result
            //         //$scope.restaurants = resp.data;
            //     }, function (err) {
            //         console.error('ERR', err);
            //         // err.status will contain the status code
            //     })
            //     return friends;
            // },
            //remove: function (chat) {
            //    chats.splice(chats.indexOf(chat), 1);
            //},
            remove: function (imageId) {
                var profileImage = $http.get(Constants.BaseURL+'/profileImage?id='+imageId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                });
                return profileImage;
            }
        }
    });
