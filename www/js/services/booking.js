angular.module('starter.services')
    .factory('Booking', function ($http, Constants, $ionicPopup, $state, $rootScope) {
        return {
            get: function (bookingid) {
                var booking = $http.get(Constants.BaseURL + '/booking/?id=' + bookingid).success(function (resp) {
                    console.log('Success booking get', resp);
                }, function (err) {
                    console.log('message', err);
                });
                return booking;
            },
            getCarInfo: function(data){
                var booking = $http.post(Constants.BaseURL + '/driver/getcarinfo',{type: data});

                return booking;

            },
            all: function () {
                var bookings = $http.get(Constants.BaseURL + '/booking').success(function (resp) {
                    console.log('Success booking all', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                });
                return bookings;
            },
            getByParams: function (params) {
               // var booking = $http.get(Constants.BaseURL + '/booking?'+params).success(function (resp) {
                var booking = $http.get(Constants.BaseURL + '/user/?id='+params).success(function (resp) {

                    console.log('Success booking passenger Set', resp);
                }, function (err) {
                    console.log('message', err);
                });
                return booking;
            },
            getRideList: function (params) {
               // var booking = $http.get(Constants.BaseURL + '/booking?'+params).success(function (resp) {
                var booking = $http.get(Constants.BaseURL + '/user/?id='+params).success(function (resp) {

                    console.log('Success booking passenger Set', resp);
                }, function (err) {
                    console.log('message', err);
                });
                return booking;
            },
            getCurrentBooking: function (params) {
               // var booking = $http.get(Constants.BaseURL + '/booking?'+params).success(function (resp) {
                var booking = $http.get(Constants.BaseURL + '/user/?id='+params).success(function (resp) {

                    console.log('Success booking passenger Set', resp);
                    if(resp.current_booking_id == ""){
                        return true;
                    }else{
                        return false;
                    }
                }, function (err) {
                    return err;
                });
                return booking;
            },
            getByParamsDriver: function (params) {
                // var booking = $http.get(Constants.BaseURL + '/booking?'+params).success(function (resp) {
                var booking = $http.get(Constants.BaseURL + '/driver/?id='+params).success(function (resp) {

                    console.log('Success booking passenger Set', resp);
                }, function (err) {
                    console.log('message', err);
                });
                return booking;
            },
            set: function (bookingInfo) {
                var booking = $http.post(Constants.BaseURL + '/booking/create?', bookingInfo).success(function (resp) {
                    console.log('Success booking Set', resp);
                }, function (err) {
                    console.log('message', err);
                });
                return booking;
            },
            setByPassenger: function (bookingInfo) {
                //var booking = $http.post(Constants.BaseURL + '/booking/create?passenger='+passengerId, bookingInfo)
                //    .success(function (result) {
                //        //booking = result;
                //        // console.log('success: '+JSON.stringify(result));
                //    });
                //return booking;


                var booking = $http.post(Constants.BaseURL + '/booking/setByPassenger', bookingInfo)
                    .success(function (result) {
                        //booking = result;
                        // console.log('success: '+JSON.stringify(result));
                    });
                return booking;


            },
            selectedDriver: function(info)
            {
              var booking = $http.post(Constants.BaseURL + '/booking/selectedDriver', info)
                  .success(function (result) {
                      //booking = result;
                      // console.log('success: '+JSON.stringify(result));
                  });
                return booking;
            },
            cancelByPassenger: function(info)
            {
              var booking = $http.post(Constants.BaseURL + '/booking/cancelBookingByPassenger', info)
                  .success(function (result) {
                      //booking = result;
                      // console.log('success: '+JSON.stringify(result));
                  });
                return booking;
            },
            completeByPassenger: function(info)
            {
              var booking = $http.post(Constants.BaseURL + '/booking/completeBookingByPassenger', info)
                  .success(function (result) {
                      //booking = result;
                      // console.log('success: '+JSON.stringify(result));
                  });
                return booking;
            },
            rateBooking: function(info)
            {
              var booking = $http.post(Constants.BaseURL + '/booking/rateBooking', info)
                  .success(function (result) {
                      //booking = result;
                      // console.log('success: '+JSON.stringify(result));
                  });
                return booking;
            },
            cancelByDriver: function(info)
            {
              var booking = $http.post(Constants.BaseURL + '/booking/cancelBookingByDriver', info)
                  .success(function (result) {
                      //booking = result;
                      // console.log('success: '+JSON.stringify(result));
                  });
                return booking;
            },

            setByDriver: function (driverId, bookingInfo) {
                var booking = $http.post(Constants.BaseURL + '/booking/create?driver=' + driverId, bookingInfo).success(function (resp) {
                    console.log('Success booking passenger Set', resp);
                }, function (err) {
                    console.log('message', err);
                });
                return booking;
            },
            completeByDriver: function(info)
            {
              var booking = $http.post(Constants.BaseURL + '/booking/completeBookingByDriver', info)
                  .success(function (result) {
                      //booking = result;
                      // console.log('success: '+JSON.stringify(result));
                  });
                return booking;
            },



            setByPassengerDriver: function (passengerId, driverId, bookingInfo) {
                var booking = $http.post(Constants.BaseURL + '/booking/create?passenger=' + passengerId + '&driver=' + driverId, bookingInfo).success(function (resp) {
                    console.log('Success booking passenger Set', resp);
                }, function (err) {
                    console.log('message', err);
                });
                return booking;
            },
            //setByParams: function (params) {
            //    var booking = $http.post(Constants.BaseURL + '/booking/create?'+params).success(function (resp) {
            //        console.log('Success booking passenger Set', resp);
            //    }, function (err) {
            //        console.log('message', err);
            //    });
            //    return booking;
            //},
            updateByParams: function (id,params) {
                var booking = $http.post(Constants.BaseURL + '/booking/update/'+id+'?'+params).success(function (resp) {
                    console.log('cancelled active booking', resp);
                }, function (err) {
                    console.log('message', err);
                });
                return booking;
            },
            cancelRideByPassenger: function(bookingObject)
            {
                var self = this;
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Warning',
                    template: 'Are you sure cancel booking?'
                });
                confirmPopup.then(function(res) {
                    if(res) {
                        console.log('You are sure');

                        //console.log($rootScope.currentBookingObject);
                       // if($rootScope.currentBookingObject){
                            self.cancelByPassenger({bookingId: bookingObject.id})
                                .success(function (response) {
                                    console.log(response);
                                    $rootScope.mapReset();
                                    $state.go('app.passenger_home');

                                }).error(function(err){

                                });
                        //}
                    } else {
                        console.log('You are not sure');
                    }
                });
            },
            isDriverFree: function(driverId)
            {
               var driver =  $http.get(Constants.BaseURL +  '/driver?id=' + driverId)
                    .success(function(response){

                    });
                return driver;
            }
        }
    });